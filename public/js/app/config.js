$(function() {
	$(":input").inputmask();

  $('.date').datepicker({
    language: "pt-BR",
    format: "dd/mm/yyyy",
    orientation: "top left",
    autoclose: true,
    todayHighlight: true
  });

   //masmoney
   $(".money").maskMoney({
       prefix: 'R$'
   });

  //Flat blue color scheme for iCheck
  $('input[type="checkbox"].flat-blue').iCheck({
    checkboxClass: 'icheckbox_flat-blue'
  });

  $('input[type="radio"].minimal-blue').iCheck({
    radioClass: 'iradio_minimal-blue'
  });

});
