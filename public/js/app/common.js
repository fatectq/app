var common = (function(window, document, $){

  return {
    init: function() {
      this.selectedSearch();
    },

    selectedSearch: function() {
      $('#input-search').bind('typeahead:selected', function(obj, result, name) {
        var url = $(this).data('url');
        url = window.ROUTES[url];
        window.location = url.replace("0", result.id);
      });
    }
  };

})(window, document, jQuery);

$(function() {
  common.init();
});
