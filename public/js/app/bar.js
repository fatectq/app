var bar = (function(window, document, $){

  return {
    init: function() {
      this.addUser();
      this.removeUser();
      this.adminUser();
    },

    showMessageError: function(id) {
      $('#' + id).fadeIn("slow");
      setTimeout(function(){ $('#' + id).fadeOut("slow") }, 5000);
    },

    addUser: function() {
      $('#form-add-user-bar').on('submit', function(e){
        var data = $(this).serialize();

        e.preventDefault();
        console.log('1');
        $.ajax({
          url: window.ROUTES.add_user_bar,
          type: 'POST',
          data: data
        }).done(function (data) {
          $('#users-bar').append(
            "<tr class='bar-user' id='" + data.user.id + "'>" +
              "<td class='name'>" + data.user.name + "</td>" +
              "<td>" +
                "<div class='btn-group' style='float:right;'>" +
                  "<a href='#' class='btn btn-sm btn-default admin-user' data-email='" + data.user.email + "' data-user='" + data.user.id + "'><i class='fa fa-plus'></i></a>" +
                  "<a href='#' class='btn btn-sm btn-default remove-user' data-email='" + data.user.email + "' data-user='" + data.user.id + "'><i class='fa fa-trash-o'></i></a>" +
                "</div>" +
              "</td>" +
            "</tr>").fadeIn('slow');
          $('#input-user').val("");
        }).error(function () {
          bar.showMessageError('error-add-user');
        });
      });
    },

    removeUser: function() {
      $(document).on('click', '.remove-user', function(e){
        var user = $(this).data('user');
        var email = $(this).data('email');
        data = {
          'bar' : $('#bar-id').val(),
          'user' : email,
          '_token' : $('#token').val()
        };
        e.preventDefault();
        $.ajax({
          url: window.ROUTES.remove_user_bar,
          type: 'POST',
          data: data
        }).done(function (data) {
          if ( data.success ) {
            $('#' + user).remove();
          } else {
            bar.showMessageError('error-admin-user');
          }
        }).error(function () {
          bar.showMessageError('error-admin-user');
        });
      });
    },

    adminUser: function() {
      $(document).on('click', '.admin-user', function(e){
        var user = $(this).data('user');
        var email = $(this).data('email');
        data = {
          'bar' : $('#bar-id').val(),
          'user' : email,
          '_token' : $('#token').val()
        };
        e.preventDefault();
        $.ajax({
          url: window.ROUTES.admin_user_bar,
          type: 'POST',
          data: data
        }).done(function (data) {
          var t = $('#' + user).find('.admin-user');
          if ( t.hasClass('btn-default') ) {
            t.removeClass('btn-default');
            t.addClass('btn-success');
          } else {
            t.removeClass('btn-success');
            t.addClass('btn-default');
          }
        }).error(function () {
          bar.showMessageError('error-remove-user');
        });
      });
    }
  };

})(window, document, jQuery);

$(function() {
  bar.init();
});
