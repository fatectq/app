var order = (function(window, document, $){

  var cart = [];

  return {
    init: function() {

      this.add();
      this.remove();
      this.plus();
      this.minus();
    },

    addCart: function(id) {
      cart.push(id);
    },

    add: function() {
      $('#input-search').bind('typeahead:selected', function(obj, result, name) {
        console.log($.inArray( result.id, cart ) < 0);
        if ( $.inArray( result.id, cart ) < 0 ) {
          order.addCart(result.id);
          $("#items").append(
            '<tr class="item" id="tr-' + result.id + '">' +
              '<td class="name" width="55%">' +
                result.result +
                '<input name="product[]" type="hidden" id="product-input-' + result.id + '" value="' + result.id + '">' +
              '</td>' +
              '<td width="15%">' +
                '<div class="input-group">' + 
                  '<span class="input-group-btn">' +
                    '<button class="btn btn-primary btn-flat btn-sm btn-minus" type="button" data-id="' + result.id + '"><i class="fa fa-minus"></i></button>' +
                  '</span>' +
                  '<small class="qtd" id="qtd-' + result.id + '">1</small>' +
                  '<input name="quantity[]" type="hidden" id="qtd-input-' + result.id + '" value="1">' +
                  '<span class="input-group-btn">' +
                    '<button class="btn btn-primary btn-flat btn-sm btn-plus" type="button" data-id="' + result.id + '"><i class="fa fa-plus"></i></button>' +
                  '</span>' +
                '</div>' +
              '</td>' +
              '<td width="20%" class="name">' +
                '<span id="value-' + result.id + '">R$ ' + result.value + '</span>' +
                '<input name="value[]" type="hidden" id="value-input-' + result.id + '" data-value="' + result.value + '" value="' + result.value + '">' +
              '</td>' +
              '<td width="10%">' +
                '<div class="btn-group" style="float:right;">' +
                  '<a href="#" class="btn btn-sm btn-danger remove-item" data-id="' + result.id + '"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
              '</td>' +
            '</tr>');
          $(this).val('');
        } else {
          var inputQtd = $("#qtd-input-" + result.id);
          var qtd = parseInt(inputQtd.val()) + 1;
          order.changeQtd(result.id, qtd);
        }
      });
      $('#input-search').bind('blur', function(){
        $(this).val('');
      });
    },

    remove: function() {
      $(document).on('click', '.remove-item', function(e){
        var id = $(this).data('id');
        $("#tr-" + id).remove();
      });
    },

    plus: function() {
      $(document).on('click', '.btn-plus', function(e){
        var id = $(this).data('id');
        var inputQtd = $("#qtd-input-" + id);
        var qtd = parseInt(inputQtd.val()) + 1;
        order.changeQtd(id, qtd);
      });
    },

    minus: function() {
      $(document).on('click', '.btn-minus', function(e){
        var id = $(this).data('id');
        var inputQtd = $("#qtd-input-" + id);
        var qtd = parseInt(inputQtd.val()) - 1;
        order.changeQtd(id, qtd);
      });
    },

    changeQtd: function(id, qtd) {
      var inputQtd = $("#qtd-input-" + id);
        var valueQtd = $("#qtd-" + id);
        var totalValue = $("#value-input-" + id);
        var inputValue = $("#value-" + id);
        var value = parseFloat(totalValue.data('value'));
        if ( qtd < 1 ) {
          qtd = 1;
        };
        valueQtd.html(qtd);
        inputValue.html('R$ ' + (qtd * value).toFixed(2) );
        inputQtd.val( qtd );
        totalValue.val( qtd * value );
    }

  };

})(window, document, jQuery);

$(function() {
  order.init();
});
