var global = (function(window, document, $){

  return {
    init: function() {
      this.typeahead();
    },

    typeahead: function() {
      // Typeahead
      // ---------
      var inputSearch = $('.typeahead');

      if (inputSearch.length > 0) {
        var itens = new Bloodhound({
          datumTokenizer: function(d) {
            return d.result;
          },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          remote: inputSearch.data('url-search'),
          displayKey: 'result',
          limit: 100000
        });

        itens.initialize();

        inputSearch.typeahead({
          highlight: false,
          hint: false,
        }, {
          name: 'item-suggest',
          displayKey: 'result',
          source: itens.ttAdapter()
        });

        inputSearch.removeAttr('style');
        $('.twitter-typeahead').removeAttr('style');
        $('.tt-dropdown-menu').removeAttr('style');
      }
    }
  };

})(window, document, jQuery);

$(function() {
  global.init();
});
