var movement = (function(window, document, $){

  return {
    init: function() {

      $('#form-movement').on('submit', function(event){
        event.preventDefault();
        $.ajax({
          url: window.ROUTES.add_movement,
          type: 'post',
          dataType: 'json',
          data: $(this).serialize()
        }).done(function (data) {
          console.log(data);
          $('#moviments-modal').modal('hide');
          movement.clear();
        });
      });

      $('#moviments-modal').on('hidden.bs.modal', function (e) {
        if ( ( $(this).attr('aria-hidden') == "true" ) ) {
          movement.clear();
        };
      });

    },

    clear: function() {
      $('#form-movement').get(0).reset();
      $('#form-movement input[type="checkbox"], #form-movement input[type="radio"]').iCheck('check');
      $('#form-movement input[type="checkbox"], #form-movement input[type="radio"]').iCheck('uncheck');
      $('#continue-repeat, #fixed-repeat').hide();
    },
  };

})(window, document, jQuery);

$(function() {
  movement.init();
});
