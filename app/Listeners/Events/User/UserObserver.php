<?php namespace nightcheers\Listeners\Events\User;

use Mail;

class UserObserver {

	/**
	 * creating
	 * Method for pre-create model object
	 * @param  Illuminate\Database\Eloquent\Model $model
	 * @return void
	 * @author Gustavo Henrique
	 */
  public function creating($model)
  {
  	$model->password = bcrypt($model->password);
  }

	/**
	 * creating
	 * Method for pos-create model object
	 * @param  Illuminate\Database\Eloquent\Model $model
	 * @return void
	 * @author Gustavo Henrique
	 */
  public function created($model)
  {
  	/*Mail::send('emails.auth.confirm', ['user' => $model->toArray()], function($message) use ($model)
		{
	    $message->to($model->email, $model->name)->subject(trans('messages.auth.confirmation.subject'));
		});*/
  }

}