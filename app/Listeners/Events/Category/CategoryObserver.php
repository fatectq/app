<?php namespace nightcheers\Listeners\Events\Category;

class CategoryObserver {

	/**
	 * deleting
	 * Method for pre-delete model object
	 * @param  Illuminate\Database\Eloquent\Model $model
	 * @return void
	 * @author Gustavo Henrique
	 */
  public function deleting($model)
  {
  	$model->category_id = null;
  	$model->save();

  	foreach ($model->categories as $category) {
  		$category->category_id = null;
  		$category->save();
  	}
  }

}