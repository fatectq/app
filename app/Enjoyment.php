<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Enjoyment extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'enjoyment';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['client_id', 'order_id', 'note'];
}
