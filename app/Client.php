<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clients';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * bar
	 * Return the bar
	 * @return nightcheers\Bar
	 * @author Gustavo Henrique
	 * 
	 */
	public function bar()
	{
		return $this->belongsTo('nightcheers\Bar');
	}

	/**
	 * orders
	 * Return the bar
	 * @return nightcheers\Bar
	 * @author Gustavo Henrique
	 * 
	 */
	public function orders()
	{
		return $this->hasMany('nightcheers\Order', 'client_id');
	}
}
