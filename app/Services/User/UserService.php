<?php namespace nightcheers\Services\User;

use nightcheers\Services\Base\BaseService;
use nightcheers\Services\Bar\BarServiceInterface;
use nightcheers\Repositories\User\UserRepositoryInterface;
use nightcheers\Repositories\Bar\BarRepositoryInterface;

class UserService extends BaseService implements UserServiceInterface{

	private $barService;

	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct(UserRepositoryInterface $repository, BarServiceInterface $barService)
	{
		$this->repository = $repository;
		$this->barService = $barService;
		parent::__construct();
	}

	/**
	 * hasAccount
	 * Return whether the user has any account associated to it
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function hasBar()
	{
		return $this->repository->getTotalBars($this->user) > 0;
	}


	/**
	 * setAccount
	 * Set the bar default
	 * @return economizee\bar
	 * @author Gustavo Henrique
	 */
	public function setBar()
	{
		$this->barService->setUser($this->user);
		return $this->barService->getDefaultBar();
	}

	/**
	 * getByLike
	 * Return the result wiht like column
	 * @param  String $value  
	 * @param  String $bar 
	 * @return Illuminate\Database\Collection
	 * @author Gustavo Henrique
	 */
	public function getByLike($value, $bar)
	{
		$withouts = [];

		foreach ($bar->users as $user) {
			$withouts[] = $user->email;
		}

		return $this->repository->getByLike('email', $value, $withouts);
	}

	/**
	 * getByAddBar
	 * Return the result wiht like column for add an bar
	 * @param  String $value  
	 * @param  String $id
	 * @return Illuminate\Database\Collection
	 * @author Gustavo Henrique
	 */
	public function getByAddBar($value, $id)
	{
		$bar = $this->barService->findById( $id );
		return $this->getByLike($value, $bar);
	}

	/**
	 * findByEmail
	 * Return a user by email
	 * @param  String $email
	 * @return economizee\User
	 * @author Gustavo Henrique
	 */
	public function findByEmail($email)
	{
		return $this->repository->findByEmail($email);
	}

}