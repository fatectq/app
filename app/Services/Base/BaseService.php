<?php namespace nightcheers\Services\Base;

use Auth;
use nightcheers\User;

abstract class BaseService implements BaseServiceInterface {

	/**
	 * repository object
	 * @var nightcheers\Repositories\Base\BaseRepositoriesInterface
	 */
	protected $repository;

	/**
	 * user object
	 * @var nightcheers\User
	 */
	protected $user;

	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct()
	{
		$user = Auth::user();
		if ( $user instanceof User ) {
			$this->setUser($user);
		}
	}

	/**
	 * create
	 * Method for create a registry
	 * @param  array  $attrs 
	 * @return Illuminate\Database\Eloquent\Model
	 * @author Gustavo Henrique
	 */
	public function create(array $attrs)
	{
		return $this->repository->create($attrs);
	}

	/**
	 * getAll
	 * Method for return all registries
	 * @return Array
	 * @author Gustavo Henrique
	 */
	public function getAll()
	{
		return $this->repository->getAll();
	}

	/**
	 * setUser
	 * Set the user
	 * @param User $user
	 * @author Gustavo Henrique
	 */
	public function setUser(User $user)
	{
		$this->user = $user;
	}

	/**
	 * getByLike
	 * Return the result wiht like column
	 * @param  String $column 
	 * @param  String $value  
	 * @return Illuminate\Database\Collection
	 * @author Gustavo Henrique
	 */
	public function getByLike($column, $value)
	{
		return $this->repository->getByLike($column, $value);
	}

}