<?php namespace nightcheers\Services\Base;

interface BaseServiceInterface {

	public function create(array $attrs);
	
}