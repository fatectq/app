<?php namespace nightcheers\Services\Bar;

use nightcheers\Services\Base\BaseService;
use nightcheers\Repositories\Bar\BarRepositoryInterface;
use Session;
use stdClass;

class BarService extends BaseService implements BarServiceInterface{
	
	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct(BarRepositoryInterface $repository)
	{
		$this->repository = $repository;
		parent::__construct();
	}
	
	/**
	 * create
	 * Method for create a registry
	 * @param  array  $attrs 
	 * @return Illuminate\Database\Eloquent\Model
	 * @author Gustavo Henrique
	 */
	public function create(array $attrs)
	{
		$attrs = array_add($attrs, 'owner_id', $this->user->id);
		$bar = $this->repository->create($attrs);
		if ( !is_null($bar) ) {
			$this->addUser($bar, null, true);
			$this->setDefaultBar($bar);
		}
		return $bar;
	}
	
	/**
	 * update
	 * Method for update a registry
	 * @param  array  $attrs 
	 * @param  String  $slug 
	 * @return Illuminate\Database\Eloquent\Model
	 * @author Gustavo Henrique
	 */
	public function update(array $attrs, $id)
	{
		if ( $this->repository->update($this->findById($id), $attrs) ) {
			return true;
		}
		return false;
	}
	
	/**
	 * destroy
	 * Method for destroy a registry
	 * @param  array  $attrs 
	 * @param  String  $slug 
	 * @return Illuminate\Database\Eloquent\Model
	 * @author Gustavo Henrique
	 */
	public function destroy($slug)
	{
		$bar = $this->findBySlug($slug);
		if ( $bar->id === $this->getDefaultBar()->id ) {
			$this->setDefaultBarForAll( $bar );
		}
		return $this->repository->delete($bar);
	}

	/**
	 * addUser
	 * Method for add a user in a bar
	 * @param  nightcheers\bar $bar 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function addUser($bar, $user = null, $admin = false)
	{
		$user = is_null($user) ? $this->user : $user;
		return $this->repository->addUser($bar, $user, $admin);
	}

	/**
	 * giveAdmin
	 * give a user admin power for bar
	 * @param  nightcheers\bar $bar 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function giveAdmin($bar, $user)
	{
		return $this->repository->giveAdmin($bar, $user);
	}

	/**
	 * removeUser
	 * Method for remove a user in a bar
	 * @param  nightcheers\bar $bar 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function removeUser($bar, $user)
	{
		return $this->repository->removeUser($bar, $user);
	}

	/**
	 * getAllbars
	 * Return the all bars for the user logged
	 * @return Illuminate\Database\Eloquent\Collection
	 * @author Gustavo Henrique
	 */
	public function getAllBars()
	{
		return $this->user->bars;
	}

	/**
	 * getAllUsers
	 * Return the all users for the bar
	 * @param nightcheers\bar $bar 
	 * @return Illuminate\Database\Eloquent\Collection
	 * @author Gustavo Henrique
	 */
	public function getAllUsers($bar)
	{
		return $bar->users;
	}

	/**
	 * getTotalbars
	 * Return the total accounts user
	 * @return Int
	 * @author Gustavo Henrique
	 */
	public function getTotalBars()
	{
		return $this->getAllBars()->count();
	}

	/**
	 * setDefaultBar
	 * Set the default bar when the user login
	 * @param nightcheers\bar $bar 
	 * @return  boolean
	 * @author Gustavo Henrique
	 */
	public function setDefaultBar($bar)
	{
		$this->repository->removeDefault($this->user);
		$default = $this->repository->setDefaultBar($bar, $this->user);
		Session::put('app.bar', $bar->id);
		return $default;
	}

	/**
	 * setDefaultBarForAll
	 * Set another default bar for all user then have this bar
	 * @param nightcheers\bar $bar 
	 * @return  boolean
	 * @author Gustavo Henrique
	 */
	public function setDefaultBarForAll($bar)
	{
		foreach ($this->repository->getAllUsers($bar) as $user) {
			$other = $this->repository->getOtherBar($bar, $user);
			if ( !is_null($other) ) {
				$this->repository->setDefaultBar($other, $user);
				if ( $this->user->id === $user->id ) {
					$this->setDefaultBar( $other );
				}
			}
		}
	}

	/**
	 * getDefaultBar
	 * Return the default bar
	 * @return nightcheers\bar
	 * @author Gustavo Henrique
	 */
	public function getDefaultBar()
	{
		if ( Session::has('app.bar') ) {
			return $this->repository->findById(Session::get('app.bar'));
		}

		return $this->setDefaultBar( $this->repository->getDefaultBar($this->user) );
	}

	/**
	 * change
	 * Change the bar for user
	 * @param  String $slug
	 * @return nightcheers\bar
	 * @author Gustavo Henrique
	 */
	public function change($id)
	{
		return $this->setDefaultBar( $this->repository->findById($id) );
	}

	/**
	 * findById
	 * Return the account by id
	 * @param  String $id
	 * @return nightcheers\Account
	 * @author Gustavo Henrique
	 */
	public function findById($id)
	{
		return $this->repository->findById($id);
	}

}