<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tables';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'seats'];

	/**
	 * bar
	 * Return the categories associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function bar()
	{
		return $this->belongsTo('nightcheers\Bar');
	}

}
