<?php namespace nightcheers\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use nightcheers\Listeners\Events\User\UserObserver;
use nightcheers\Listeners\Events\Category\CategoryObserver;
use nightcheers\User;
use nightcheers\Category;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'event.name' => [
			'EventListener',
		],
	];

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot(Dispatcher $events)
	{
		parent::boot($events);
		$this->observerModels();
	}

	/**
	 * observerModels
	 * Set the events for models
	 * @return void
	 * @author Gustavo Henrique
	 */
	private function observerModels()
	{
		User::observe(new UserObserver);
		Category::observe(new CategoryObserver);
	}

}
