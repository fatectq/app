<?php namespace nightcheers\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposersServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		View::composer('layouts.normal.*', 'nightcheers\Http\Composers\AppComposer');
	}

	/**
	 * Overwrite any vendor / package configuration.
	 *
	 * This service provider is intended to provide a convenient location for you
	 * to overwrite any "vendor" or package configuration that you may want to
	 * modify before the application handles the incoming request / command.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
