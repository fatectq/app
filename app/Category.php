<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'category_id'];

	/**
	 * categories
	 * Return the categories associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function categories()
	{
		return $this->hasMany('nightcheers\Category', 'category_id');
	}

	/**
	 * parent
	 * Return the parent category
	 * @return nightcheers\Category
	 * @author Gustavo Henrique
	 * 
	 */
	public function parent()
	{
		return $this->belongsTo('nightcheers\Category');
	}
}
