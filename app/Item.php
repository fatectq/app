<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'items';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['order_id', 'product_id', 'price', 'quantity'];

	/**
	 * product
	 * Return the product
	 * @return nightcheers\Product
	 * @author Gustavo Henrique
	 * 
	 */
	public function product()
	{
		return $this->belongsTo('nightcheers\Product');
	}

	/**
	 * order
	 * Return the order
	 * @return nightcheers\Order
	 * @author Gustavo Henrique
	 * 
	 */
	public function order()
	{
		return $this->belongsTo('nightcheers\Order');
	}
}
