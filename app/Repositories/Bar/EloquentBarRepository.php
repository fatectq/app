<?php namespace nightcheers\Repositories\Bar;

use nightcheers\Repositories\Base\EloquentBaseRepository;
use nightcheers\Bar;
use nightcheers\User;

class EloquentBarRepository extends EloquentBaseRepository implements BarRepositoryInterface{

	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct(Bar $model)
	{
		$this->model = $model;
	}

	/**
	 * addUser
	 * Method for add a user in a bar
	 * @param  nightcheers\Bar $bar 
	 * @param  nightcheers\User $user 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function addUser(Bar $bar, User $user, $admin)
	{
		return $bar->users()->attach($user->id, ['admin' => $admin]);
	}

	/**
	 * giveAdmin
	 * give a user admin power
	 * @param nightcheers\Bar $bar 
	 * @author Gustavo Henrique
	 */
	public function giveAdmin(Bar $bar, User $user)
	{
		$admin = !$user->bars()->where('bar_id', $bar->id)->first()->pivot->admin;
		return $user->bars()->updateExistingPivot($bar->id, ['admin' => $admin]);
	}

	/**
	 * removeUser
	 * Method for remove a user in a bar
	 * @param  nightcheers\Bar $bar 
	 * @param  nightcheers\User $user 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function removeUser(Bar $bar, User $user)
	{
		return $bar->users()->detach($user->id);
	}

	/**
	 * setDefaultBar
	 * Set the default bar when the user login
	 * @param nightcheers\Bar $bar 
	 * @author Gustavo Henrique
	 */
	public function setDefaultBar(Bar $bar, User $user)
	{
		return $user->bars()->updateExistingPivot($bar->id, ['default' => true]);
	}

	/**
	 * getDefaultBar
	 * Return the default bar
	 * @return nightcheers\Bar
	 * @author Gustavo Henrique
	 */
	public function getDefaultBar(User $user)
	{
		return $user->bars()->wherePivot('default', true)->first();
	}

	/**
	 * getAllUsers
	 * Return the all active
	 * @param Illuminate\Database\Eloquent $bar
	 * @return Illuminate\Database\Eloquent
	 * @author Gustavo Henrique
	 */
	public function getAllUsers(Bar $bar)
	{
		return $bar->users()->wherePivot('default', true)->get();
	}

	/**
	 * getOtherbar
	 * Return the other bar diff the bar param
	 * @param  nightcheers\Bar $bar
	 * @param  nightcheers\User    $user    
	 * @return nightcheers\Bar
	 * @author Gustavo Henrique
	 */
	public function getOtherbar(Bar $bar, User $user)
	{
		return $user->bars()->where('bars.id', '!=',$bar->id)->first();
	}

	/**
	 * removeDefault
	 * Remove all default bar 
	 * @param nightcheers\User $user
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function removeDefault(User $user)
	{
		return $user->bars()->update(['default' => 0]);
	}
	
}