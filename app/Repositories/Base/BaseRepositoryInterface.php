<?php namespace nightcheers\Repositories\Base;

interface BaseRepositoryInterface {

	public function getAll();

	public function findById($id);

	public function save();

	public function create(array $inputs);

}