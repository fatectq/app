<?php namespace nightcheers\Repositories\Base;

abstract class EloquentBaseRepository implements BaseRepositoryInterface {

	/**
	 * model object
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * save
	 * Save the instance model
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function save()
	{
		return $this->model->save();
	}

	/**
	 * getAll
	 * Return the all registries
	 * @return Array
	 * @author Gustavo Henrique
	 */
	public function getAll()
	{
		return $this->model->all();;
	}

	/**
	 * getAllActive
	 * Return the all active
	 * @return Illuminate\Database\Collection
	 * @author Gustavo Henrique
	 */
	public function getAllActive()
	{
		return $this->getActive()->get();
	}

	/**
	 * findById
	 * Return the registry by id
	 * @return Array
	 * @author Gustavo Henrique
	 */
	public function findById($id)
	{
		return $this->model->find($id);
	}

	/**
	 * create
	 * Method for create a registry in database
	 * @param  array  $inputs 
	 * @return Illuminate\Database\Eloquent\Model
	 * @author Gustavo Henrique
	 */
	public function create(array $inputs)
	{
		return $this->model->create($inputs);
	}

	/**
	 * update
	 * Method for create a registry in database
	 * @param  array  $inputs 
	 * @param  Illuminate\Database\Eloquent\Model  $model 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function update($model, array $inputs)
	{
		return $model->update($inputs);
	}

	/**
	 * delete
	 * Method for delete a registry in database
	 * @param  Illuminate\Database\Eloquent\Model  $model 
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function delete($model)
	{
		return $model->delete();
	}

	/**
	 * getAllActive
	 * Return the all active
	 * @param  String $column
	 * @return Illuminate\Database\Eloquent
	 * @author Gustavo Henrique
	 */
	protected function getActive($column = null)
	{
		$column = ( is_null($column) ) ? 'active' : $column;
		return $this->model->where($column, 1);
	}

	/**
	 * getByLike
	 * Return the result wiht like column
	 * @param  String $column 
	 * @param  String $value  
	 * @return Illuminate\Database\Collection
	 * @author Gustavo Henrique
	 */
	public function getByLike($column, $value, array $whitout = [])
	{
		$eloquent = $this->model->where($column, 'like', '%' . $value . '%');

		if ( !empty($whitout) ) {
			$eloquent->whereNotIn($column, $whitout);
		}

		return $eloquent->get();
	}

}