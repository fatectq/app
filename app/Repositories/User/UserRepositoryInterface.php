<?php namespace nightcheers\Repositories\User;

use User;
use String;

interface UserRepositoryInterface {

	public function getTotalBars(User $user);
	
}