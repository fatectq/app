<?php namespace nightcheers\Repositories\User;

use nightcheers\Repositories\Base\EloquentBaseRepository;
use nightcheers\User;
use String;

class EloquentUserRepository extends EloquentBaseRepository implements UserRepositoryInterface{

	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct(User $model)
	{
		$this->model = $model;
	}

	/**
	 * getTotalBars
	 * Return the total bars than user has associated with it
	 * @param  nightcheers\User
	 * @return Integer
	 * @author Gustavo Henrique
	 */
	public function getTotalBars(User $user)
	{
		return $user->bars->count();
	}

	/**
	 * findByEmail
	 * Return a user by email
	 * @param  String $email
	 * @return economizee\User
	 * @author Gustavo Henrique
	 */
	public function findByEmail($email)
	{
		return $this->model->where('email', $email)->first();
	}
}