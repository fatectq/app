<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Bar extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'bars';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'owner_id'];



	/**
	 * users
	 * Return the users associated with user
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function users()
	{
		return $this->belongsToMany('nightcheers\User', 'users_has_bar')->withPivot('default', 'admin');
	}

	/**
	 * isAdmin
	 * Return if user id admin
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function isAdmin()
	{
		$user = $this->users()->where('user_id', Auth::user()->id)->first();
		if ( is_null($user) ) {
			return false;
		}
		
		return $user->pivot->admin;
	}

}
