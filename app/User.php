<?php namespace nightcheers;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * bars
	 * Return the bars associated with user
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function bars()
	{
		return $this->belongsToMany('nightcheers\Bar', 'users_has_bar')->withPivot('default', 'admin');
	}

	/**
	 * isAdmin
	 * Return if user id admin
	 * @return boolean
	 * @author Gustavo Henrique
	 */
	public function isAdmin($bar)
	{
		return $this->bars()->where('bar_id', $bar->id)->first()->pivot->admin;
	}

}
