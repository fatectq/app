<?php 

namespace nightcheers\Http\Requests\Promotion;
use nightcheers\Http\Requests\Request;

class PromotionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'client' => 'required',
			'discount' => 'required',
			'start_date' => 'required',
                        'end_date' => 'required'
		];
	}

}
