<?php namespace nightcheers\Http\Requests\Table;

use nightcheers\Http\Requests\Request;

class TableRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'seats' => 'required|max:10|integer'
		];
	}

}
