<?php namespace nightcheers\Http\Controllers\Order;

use nightcheers\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use nightcheers\Bar;
use nightcheers\Table;
use nightcheers\Product;
use nightcheers\Item;
use nightcheers\Order;
use nightcheers\Client;

class OrderController extends Controller {

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$bar = Bar::find(Session::get('app.bar'));

		$tableIds = Table::where('bar_id', $bar->id)->lists('id');

		$orders = Order::whereIn('table_id', $tableIds)->whereNull('finished_at')->get();
		
		$data = [
			'orders' => $orders,
			'indexBar' => $bar
		];

		return view('pages.order.index')->with($data);
	}

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function today()
	{
		$bar = Bar::find(Session::get('app.bar'));

		if ( !Auth::user()->isAdmin($bar) ) {
			return redirect()->route('dashboard');
		}

		$tableIds = Table::where('bar_id', $bar->id)->lists('id');

		$date = date('Y-m-d 23:59:59', strtotime("-1 days"));

		$orders = Order::whereIn('table_id', $tableIds)->where('created_at', '>', $date)->get();
		
		$data = [
			'orders' => $orders,
			'indexBar' => $bar
		];

		return view('pages.order.today')->with($data);
	}

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function create()
	{
		$bar = Bar::find(Session::get('app.bar'));

		$data = [
			'tables' => Table::where('bar_id', $bar->id)->get(),
			'indexBar' => $bar
		];
		return view('pages.order.create')->with($data);
	}

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function view($id)
	{
		$order = Order::find($id);

		$bar = Bar::find(Session::get('app.bar'));

		if ( ( $bar->id != $order->table->bar->id ) || ( !Auth::user()->isAdmin($bar) ) ) {
			return redirect()->route('dashboard');
		}

		$data = [
			'order' => $order,
			'indexBar' => $bar
		];

		return view('pages.order.view')->with($data);
	}

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::find($id);

		$bar = Bar::find(Session::get('app.bar'));

		if ( $bar->id != $order->table->bar->id ) {
			return redirect()->route('dashboard');
		} elseif ( !is_null($order->finished_at) ) {
			return redirect()->route('view_order', ['id' => $order->id]);
		}

		$data = [
			'tables' => Table::where('bar_id', $bar->id)->get(),
			'order' => $order,
			'indexBar' => $bar,
			'finished' => false
		];
		return view('pages.order.finish')->with($data);
	}

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function finish($id)
	{
		$order = Order::find($id);

		$bar = Bar::find(Session::get('app.bar'));

		if( !Auth::user()->isAdmin($bar) ) {
			return redirect()->route('bar');
		} elseif ( $bar->id != $order->table->bar->id ) {
			return redirect()->route('dashboard');
		} elseif ( !is_null($order->finished_at) ) {
			return redirect()->route('view_order', ['id' => $order->id]);
		}

		$data = [
			'tables' => Table::where('bar_id', $bar->id)->get(),
			'order' => $order,
			'indexBar' => $bar,
			'finished' => true
		];

		return view('pages.order.finish')->with($data);
	}

  /**
   * search
   * Search a product
   * @param  Request $request 
   * @param  Int  $id      
   * @return Array
   * @author Gustavo Henrique
   */
  public function search(Request $request, $id) {

  	$bar = Bar::find(Session::get('app.bar'));

		// $tableIds = Table::where('bar_id', $bar)->lists('id');

		// $orderIds = Order::whereIn('table_id', $tableIds)->whereNull('finished_at')->get()->lists('id');

    $data = [];

    $results = Client::where('bar_id', $bar->id)->where('name', 'like', '%' . $request->q . '%')->get();

    foreach ($results as $result) {
    	$order = $result->orders()->whereNull('finished_at')->first();

    	if ( !is_null($order) ) {
	      $data[] = [
					'id' => $order->id,
					'result' => $result->name
				];
    	}
    }

    return response()->json($data);
  }

	/**
	 * store
	 * Create a new order
	 * @param  Request $request 
	 * @return Void
	 * @author Gustavo Henrique
	 */
	public function store(Request $request)
	{
		$client = new Client();
		$client->bar_id = Session::get('app.bar');
		$client->name = $request->name;
		$client->save();

		$order = new Order();
		$order->table_id = $request->table;
		$order->client_id = $client->id;
		$order->token = mt_rand(0,999999);
		$order->save();

		for ($i=0; $i < count($request->product); $i++) { 
			$product = Product::find($request->product[$i]);
			$item = new Item();
			$item->order_id = $order->id;
			$item->product_id = $product->id; 
			$item->price = $product->sale_value; 
			$item->quantity = $request->quantity[$i];
			$item->save();
		}

		$order = Order::find($order->id);
		$total = 0;
		foreach ($order->items as $item) {
			$total += ($item->price * $item->quantity);
		}

		$order->total = $total;

		return redirect()->route('edit_order', ['id' => $order->id]);
	}

	/**
	 * store
	 * Create a new order
	 * @param  Request $request 
	 * @return Void
	 * @author Gustavo Henrique
	 */
	public function update(Request $request, $id)
	{
		$order = Order::find($id);
		$this->updateOrder($request, $order);
		return redirect()->route('edit_order', ['id' => $order->id]);	
	}

	/**
	 * store
	 * Create a new order
	 * @param  Request $request 
	 * @return Void
	 * @author Gustavo Henrique
	 */
	public function finishing(Request $request, $id)
	{
		$order = Order::find($id);
		$this->updateOrder($request, $order);
		$order->finished_at = date('Y-m-d h:i:s');
		$order->save();
		return redirect()->route('view_order', ['id' => $order->id]);
	}

	/**
	 * updateOrder
	 * Update a order
	 * @param  Request $request 
	 * @return void
	 * @author Gustavo Henrique
	 */
	private function updateOrder($request, $order)
	{
		$productsIds = [];

		foreach ($order->items as $item) {
			$productsIds[] = $item->product->id;
		}

		for ($i=0; $i < count($request->product); $i++) { 
			$product = Product::find($request->product[$i]);
			if ( in_array($product->id, $productsIds) ) {
				$item = Item::where('order_id', $order->id)->where('product_id', $product->id)->first();
			} else {
				$productsIds[] = $product->id;
				$item = new Item();
				$item->order_id = $order->id;
				$item->product_id = $product->id; 
			}
			$item->price = $product->sale_value;
			$item->quantity = $request->quantity[$i];
			$item->save();
		}

		$deleteIds = array_diff($productsIds, $request->product);

		Item::where('order_id', $order->id)->whereIn('product_id', $deleteIds)->delete();

		$order = Order::find($order->id);
		$total = 0;
		foreach ($order->items as $item) {
			$total += ($item->price * $item->quantity);
		}

		$order->total = $total;
		$order->save();
	}

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function visit($token)
	{
		$order = Order::where('token', $token)->first();
		
		$data = [
			'order' => $order
		];

		return view('pages.order.visit')->with($data);
	}

}
