<?php namespace nightcheers\Http\Controllers\Auth;

use nightcheers\Http\Controllers\Controller;
use nightcheers\Services\User\UserServiceInterface;
use nightcheers\Http\Requests\Auth\SingupRequest;
use nightcheers\Http\Requests\Auth\SinginRequest;
use Illuminate\Auth\Guard;
use Session;

class AuthController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserServiceInterface $service, Guard $auth)
	{
		$this->service = $service;
		$this->auth = $auth;
	}

	/**
	 * Show the page for login.
	 * @return \Illuminate\Http\Response
	 * @author  Gustavo Henrique
	 */
	public function login()
	{
		return view('pages.auth.login');
	}

	/**
	 * register
	 * Show the page for register.
	 * @return \Illuminate\Http\Response
	 * @author Gustavo Henrique
	 */
	public function register()
	{
		return view('pages.auth.register');
	}

	/**
	 * singup
	 * Create a new user
	 * @param  \Illuminate\Http\SingupRequest  $request
	 * @return \Illuminate\Http\Response
	 * @author Gustavo Henrique
	 */
	public function singup(SingupRequest $request)
	{
		$this->service->create($request->all());
		Session::flash('success_singup', trans('messages.auth.success_singup'));
		return redirect()->route('login');
	}

	/**
	 * login
	 * Handle a login request to the application.
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 * @author Gustavo Henrique
	 */
	public function singin(SinginRequest $request)
	{
		$credentials = $request->only('email', 'password');
		if ( $this->auth->attempt($credentials, $request->has('remember')) ) {
			$this->service->setUser($this->auth->getUser());
			if ( $this->service->hasBar() ) {
				$this->service->setBar();
			}
			return redirect()->route('dashboard');
		}

		return redirect()->route('login')
					->withInput($request->only('email'))
					->withErrors([
						'email' => trans('messages.auth.error_singin'),
						'password' => trans('messages.auth.error_singin'),
					]);
	}

	/**
	 * logout
	 * Log the user out of the application.
	 * @return \Illuminate\Http\Response
	 * @author Gustavo Henrique
	 */
	public function logout()
	{
		Session::forget('app.bar');
		$this->auth->logout();

		return redirect()->route('login');
	}

	/**
	 * confirm
	 * Confirm the user 
	 * @return \Illuminate\Http\Response
	 * @author Gustavo Henrique
	 */
	public function confirm($code)
	{
		if ( $this->service->confirmUser($code) ) {
			Session::flash('success_confirm', trans('messages.auth.success_confirm'));
		} else {
			Session::flash('error_confirm', trans('messages.auth.error_confirm'));
		}
		return redirect()->route('login');
	}


}
