<?php

namespace nightcheers\Http\Controllers\Product;

use nightcheers\Http\Controllers\Controller;
use Illuminate\Http\Request;
use nightcheers\Http\Requests\Product\ProductRequest;
use nightcheers\Product;
use nightcheers\Category;
use nightcheers\Bar;
use Session;

class ProductController extends Controller {

  /**
   * index
   * Show the application dashboard screen to the user.
   * @return Response
   */
  public function index() {
      $data = [
          'products' => Product::where('bar_id', '=', Session::get('app.bar'))->orderBy('category_id')->paginate(15)
      ];
      return view('pages.product.index')->with(array_merge($data, $this->basicData));
  }

  /**
   * create
   * Show the application dashboard screen to the user.
   * @return Response
   */
  public function create() {
      $data = [
          'categories' => Category::where('bar_id', '=', Session::get('app.bar'))->get(),
      ];
      return view('pages.product.create')->with($data);
  }

  /**
   * search
   * Search a product
   * @param  Request $request 
   * @param  Int  $id      
   * @return Array
   * @author Gustavo Henrique
   */
  public function search(Request $request, $id) {
      $data = [];

      $results = Product::where('bar_id', $id)->where('name', 'like', '%' . $request->q . '%')->get();

      foreach ($results as $result) {
        $data[] = [
					'id' => $result->id,
					'result' => $result->name,
					'value' => $result->sale_value
				];
      }

      return response()->json($data);
  }

  /**
   * store
   * Show the application dashboard screen to the user.
   * @return Response
   */
  public function store(ProductRequest $request) {
      $product = new Product();
      $this->_setProduct($product, $request);
      $product->save();

      return redirect()->route('products');
  }

  public function edit($id, Request $request) {
      $data = [
          'product' => Product::find($id),
          'categories' => Category::all(),
      ];
      return view('pages.product.edit')->with($data);
  }

  public function update($id, ProductRequest $request) {
      $product = Product::find($id);
      $this->_setProduct($product, $request);
      $product->save();

      return redirect()->route('products');
  }

  public function destroy($id, Request $request) {
      Product::find($id)->delete();
      return redirect()->route('products');
  }

  private function _setProduct(&$product, $request) {
      $product->bar_id = Session::get('app.bar');
      $product->name = $request->input('name');
      $product->purchase_value = str_replace('R$', '', $request->input('purchase_value'));
      $product->sale_value = str_replace('R$', '', $request->input('sale_value'));
      $product->category_id = $request->input('category');
  }

}
