<?php namespace nightcheers\Http\Controllers\Dashboard;

use nightcheers\Http\Controllers\Controller;
use nightcheers\Table;
use DB;
use Session;

class DashboardController extends Controller {

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
         * teste aqui
	 */
	public function index()
	{
		$initDate = date('Y-m-01');

    $query = DB::table('orders')
    						->where('created_at', '>=', $initDate);

    $total = $query->sum('total');

    $numberOrders = $query->count();

    $tableIds = Table::where('bar_id', Session::get('app.bar'))->lists('id');

    if ( count($tableIds) != 0 ) {
      $tableIds = implode(',', $tableIds);
      $graphs = DB::select(DB::raw("select sum(total) total, month(created_at) mes from orders
                                      where table_id in(" . $tableIds . ") group by month(created_at) 
                                      order by month(created_at)"));
    } else {
      $graphs = [];
    }

    $resultGraph = [];

    foreach ($graphs as $graph) {
    	$resultGraph[$graph->mes] = (int)$graph->total;
    }

    $keys = array_keys($resultGraph);

    for ($i=1; $i < 13; $i++) { 

    	if( !in_array($i, $keys) )
    		$resultGraph[$i] = 0;
    }

    ksort($resultGraph);

    $data = [
	    'total' => $total,
	    'numberOrders' => $numberOrders,
	    'graph' => $resultGraph
	  ];

		return view('pages.dashboard.index')->with($data);
	}

}
