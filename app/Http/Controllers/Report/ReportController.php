<?php namespace nightcheers\Http\Controllers\Report;

use nightcheers\Http\Controllers\Controller;
use nightcheers\Services\User\UserServiceInterface;
use Illuminate\Http\Request;
use Session;
use nightcheers\Bar;
use nightcheers\Product;
use nightcheers\Promotion;
use nightcheers\Order;
use nightcheers\Table;

class ReportController extends Controller {


	/**
	 * order
	 * Report for order
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function order()
	{
		$data = [
			'orders' => [],
			'printUrl' => ''
		];
		return view('pages.report.order')->with($data);
	}

	/**
	 * promotion
	 * Report for promotion
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function promotion()
	{
		$data = [
			'promotions' => [],
			'printUrl' => ''
		];
		return view('pages.report.promotion')->with($data);
	}

	/**
	 * product
	 * Report for product
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function product()
	{
		$data = [
			'products' => [],
			'printUrl' => ''
		];
		return view('pages.report.product')->with($data);
	}

	/**
	 * order
	 * Report for order
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function researchOrder(Request $request)
	{
		$params = array_merge($request->except('_token'), ['type' => 'order']);

		$data = [
			'orders' => $this->getReportOrder($request),
			'printUrl' => route('report_print', $params)
		];

		return view('pages.report.order')->with($data);
	}

	/**
	 * promotion
	 * Report for promotion
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function researchPromotion(Request $request)
	{
		$params = array_merge($request->except('_token'), ['type' => 'promotion']);

		$data = [
			'promotions' => $this->getReportPromotion($request),
			'printUrl' => route('report_print', $params)
		];

		return view('pages.report.promotion')->with($data);
	}

	/**
	 * product
	 * Report for product
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function researchProduct(Request $request)
	{
		$params = array_merge($request->except('_token'), ['type' => 'product']);

		$data = [
			'products' => $this->getReportProducts($request),
			'printUrl' => route('report_print', $params)
		];

		return view('pages.report.product')->with($data);
	}

	/**
	 * product
	 * Report for product
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function printer(Request $request)
	{
		$data = [
			'products' => $this->getReportProducts($request),
			'promotions' => $this->getReportPromotion($request),
			'orders' => $this->getReportOrder($request),
			'type' => $request->type
		];

		return view('pages.report.print')->with($data);
	}

	/**
	 * getReportProducts
	 * Return the products for report
	 * @param  Request $request
	 * @return Array
	 * @author Gustavo Henrique
	 */
	private function getReportProducts($request)
	{
		$bar = bar::find(Session::get('app.bar'));

		$model = Product::where('bar_id', $bar->id);

		if ( $request->has('date-min') && !empty($request->input('date-min')) ) {
			$model->where('created_at', '>=', $request->input('date-min'));
		}

		if ( $request->has('date-max') && !empty($request->input('date-max')) ) {
			$model->where('created_at', '<=', $request->input('date-max'));
		}

		if ( $request->has('value-sale-min') && !empty($request->input('value-sale-min')) ) {
			$model->where('sale_value', '>=', $request->input('value-sale-min'));
		}

		if ( $request->has('value-sale-max') && !empty($request->input('value-sale-max')) ) {
			$model->where('sale_value', '<=', $request->input('value-sale-max'));
		}

		if ( $request->has('value-purchase-min') && !empty($request->input('value-purchase-min')) ) {
			$model->where('purchase_value', '>=', $request->input('value-purchase-min'));
		}

		if ( $request->has('value-purchase-max') && !empty($request->input('value-purchase-max')) ) {
			$model->where('purchase_value', '<=', $request->input('value-purchase-max'));
		}

		return $model->get();
	}

	private function getReportPromotion($request)
	{
		$bar = bar::find(Session::get('app.bar'));

		$model = Promotion::where('bar_id', $bar->id);

		if ( $request->has('date-min') && !empty($request->input('date-min')) ) {
			$model->where('created_at', '>=', $request->input('date-min'));
		}

		if ( $request->has('date-max') && !empty($request->input('date-max')) ) {
			$model->where('created_at', '<=', $request->input('date-max'));
		}

		return $model->get();
	}

	private function getReportOrder($request)
	{
		$bar = Bar::find(Session::get('app.bar'));

		$ids = Table::where('bar_id', $bar->id)->get()->lists('id');

		$model = Order::whereIn('table_id', $ids);

		if ( $request->has('date-min') && !empty($request->input('date-min')) ) {
			$model->where('created_at', '>=', $request->input('date-min'));
		}

		if ( $request->has('date-max') && !empty($request->input('date-max')) ) {
			$model->where('created_at', '<=', $request->input('date-max'));
		}

		if ( $request->has('value-min') && !empty($request->input('value-min')) ) {
			$model->where('total', '>=', $request->input('value-min'));
		}

		if ( $request->has('value-max') && !empty($request->input('value-max')) ) {
			$model->where('total', '<=', $request->input('value-max'));
		}

		return $model->get();
	}
}