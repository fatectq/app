<?php

namespace nightcheers\Http\Controllers\Promotion;

use nightcheers\Http\Controllers\Controller;
use Illuminate\Http\Request;
use nightcheers\Http\Requests\Promotion\PromotionRequest;
use nightcheers\Client;
use nightcheers\Promotion;
use Session;

class PromotionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $data = [
            'promotions' => Promotion::where('bar_id', '=', Session::get('app.bar'))->get()
        ];
        return view('pages.promotion.index')->with(array_merge($data, $this->basicData));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        $data = [
            'clients' => Client::where('bar_id', '=', Session::get('app.bar'))->get(),
            'promotion' => null
        ];
        return view('pages.promotion.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PromotionRequest $request) {
        $promotion = new Promotion();
        $this->_setPromotion($promotion, $request);
        $promotion->save();
        return redirect()->route('promotions');
    }

    private function _setPromotion($promotion, $request) {
        $promotion->bar_id = Session::get('app.bar');
        $promotion->client_id = $request->input('client');
        $promotion->discount = $request->input('discount');
        $promotion->start_date = implode("-", array_reverse(explode("/", $request->input('start_date'))));
        $promotion->end_date = implode("-", array_reverse(explode("/", $request->input('end_date'))));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $data = [
            'promotion' => Promotion::find($id)
        ];
        return view('pages.promotion.edit')->with($data);
    }

    /**
     * search
     * Search a product
     * @param  Request $request 
     * @param  Int  $id      
     * @return Array
     * @author Gustavo Henrique
     */
    public function search(Request $request, $id) {
        $ids = Promotion::where('bar_id', $id)->get()->lists('client_id');
        
        $data = [];

        $results = Client::whereIn('id', $ids)->where('email', 'like', '%' . $request->q . '%')->get();

        foreach ($results as $result) {
            $id = Promotion::where('client_id', $result->id)->where('bar_id', Session::get('app.bar'))->first()->id;
            $data[] = ['result' => $result->name, 'id' => $id];
        }

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, PromotionRequest $request) {
        $promotion = Promotion::find($id);
        $this->_setPromotion($promotion, $request);
        $promotion->save();

        return redirect()->route('promotions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Promotion::find($id)->delete();
        return redirect()->route('promotions');
    }

}
