<?php namespace nightcheers\Http\Controllers\User;

use nightcheers\Http\Controllers\Controller;
use nightcheers\Services\User\UserServiceInterface;
use Illuminate\Http\Request;

class UserController extends Controller {

	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct(UserServiceInterface $service)
	{
		$this->service = $service;
	}

	/**
	 * Show the all accounts screen.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function search(Request $request, $id)
	{
		$data = [];

		$users = $this->service->getByAddBar($request->q, $id);

		foreach ($users as $user) {
			$data[] = ['result' => $user->email];
		}

		return response()->json($data);
	}
}
