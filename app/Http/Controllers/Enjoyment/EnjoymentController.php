<?php namespace nightcheers\Http\Controllers\Enjoyment;

use nightcheers\Http\Controllers\Controller;
use Illuminate\Http\Request;
use nightcheers\Enjoyment;
use nightcheers\Bar;
use nightcheers\Order;
use Session;

class EnjoymentController extends Controller {

	/**
	 * index
	 * Show the application dashboard screen to the user.
	 * @return Response
	 */
	public function index($token)
	{
		$order = Order::where('token', $token)->first();

		$data = [
			'order' => $order
		];

		return view('pages.enjoyment.index')->with($data);
	}

	/**
	 * store
	 * Show the application dashboard screen to the user.
	 * @return Response
	 */
	public function store(Request $request, $token)
	{
		$order = Order::where('token', $token)->first();

		$enjoyment = new Enjoyment();
		$enjoyment->client_id = $order->client->id;
		$enjoyment->order_id = $order->id;
		$enjoyment->note = $request->note;
		$enjoyment->save();

		return redirect()->route('enjoyment', ['token' => $token]);
	}

	/**
	 * store
	 * Show the application dashboard screen to the user.
	 * @return Response
	 */
	public function email(Request $request, $token)
	{
		$order = Order::where('token', $token)->first();

		$client = $order->client;
		$client->email = $request->email;
		$client->save();

		return response()->json(['success' => true]);
	}

}
