<?php namespace nightcheers\Http\Controllers\Table;

use nightcheers\Http\Controllers\Controller;
use Illuminate\Http\Request;
use nightcheers\Http\Requests\Table\TableRequest;
use nightcheers\Table;
use nightcheers\Bar;
use Session;


class TableController extends Controller {

	/**
	 * Show the application dashboard screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = [
			'tables' => Table::where('bar_id', '=', Session::get('app.bar'))->get()
		];
		return view('pages.tables.index')->with(array_merge($data, $this->basicData));
	}

	/**
	 * search
	 * Search a product
	 * @param  Request $request 
	 * @param  Int  $id      
	 * @return Array
	 * @author Gustavo Henrique
	 */
	public function search(Request $request, $id)
	{
		$data = [];

		$results = Table::where('bar_id', $id)->where('name', 'like', '%' . $request->q . '%')->get();

		foreach ($results as $result) {
			$data[] = ['result' => $result->name, 'id' => $result->id];
		}
		
		return response()->json($data);
	}

	public function create()
	{
		$data = [
			'tables' => Table::all()
		];
		return view('pages.tables.create')->with($data);
	}

	public function store(TableRequest $request)
	{
		$table = new Table();
		$table->name = $request->input('name');
		$table->seats = $request->input('seats');
		$table->bar_id = Session::get('app.bar');
		if ( $request->input('table') != 0 ) {
			$table->table_id = $request->input('table');
		}
		$table->save();

		return redirect()->route('tables');
	}


	public function edit($id, Request $request)
	{
		$data = [
			'tables' => Table::find($id)
		];
		return view('pages.tables.edit')->with($data);
	}

	public function update($id, TableRequest $request)
	{
		$table = Table::find($id);
		$table->name = $request->input('name');
		$table->seats = $request->input('seats');
		$table->bar_id = Session::get('app.bar');
		if ( $request->input('table') != 0 ) {
			$table->table_id = $request->input('table');
		}
		$table->save();

		return redirect()->route('tables');
	}

	public function destroy($id, Request $request)
	{
		Table::find($id)->delete();
		return redirect()->route('tables');
	}

}
