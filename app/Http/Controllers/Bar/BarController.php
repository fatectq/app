<?php namespace nightcheers\Http\Controllers\Bar;

use nightcheers\Http\Controllers\Controller;
use nightcheers\Services\Bar\BarServiceInterface;
use nightcheers\Services\User\UserServiceInterface;
use nightcheers\Http\Requests\Bar\StoreRequest;
use nightcheers\Http\Requests\Bar\UserRequest;
use Illuminate\Http\Request;
use nightcheers\Bar;
use Auth;

class BarController extends Controller {

	private $userService;

	/**
	 * __construct
	 * The construct of object
	 */
	public function __construct(BarServiceInterface $service, UserServiceInterface $userService)
	{
		$this->service = $service;
		$this->userService = $userService;
	}

	/**
	 * Show the all accounts screen.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function index()
	{
		$data = [
			'indexBars' => Auth::user()->bars
		];
		return view('pages.bar.index')->with($data);
	}

	/**
	 * create
	 * Show the account screen.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function create()
	{
		return view('pages.bar.create');
	}

	/**
	 * edit
	 * Show the account edit screen.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function edit(Request $request, $id)
	{
		$bar = Bar::find($id);

		if ( !$bar->isAdmin() ) {
			return redirect()->route('bars');
		}

		$data = [
			'editBar' => $bar,
			'usersBar' => $this->service->getAllUsers($bar)
		];
		return view('pages.bar.edit')->with($data);
	}

	/**
	 * store
	 * Persist the new account .
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function store(StoreRequest $request)
	{
		$this->service->create($request->except('_token'));
		return redirect()->route('dashboard');
	}

	/**
	 * update
	 * Update the account.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function update(StoreRequest $request, $id)
	{
		$this->service->update($request->except('_token'), $id);
		return redirect()->route('bars');
	}

	/**
	 * change
	 * Change the account
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function change(Request $request, $id)
	{
		$this->service->change($id);
		return redirect()->route('dashboard');
	}

	/**
	 * destroy
	 * Destroiy the account
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function destroy(Request $request, $id)
	{
		$this->service->destroy($id);
		return redirect()->route('dashboard');
	}

	/**
	 * addUser
	 * Add user in a account.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function addUser(UserRequest $request)
	{
		$bar = $this->service->findById( $request->bar );
		$user = $this->userService->findByEmail( $request->user );
		$this->service->addUser($bar, $user, false);
		return response()->json(['user' =>  $user]);
	}

	/**
	 * giveAdmin
	 * give a user admin power for bar
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function giveAdmin(UserRequest $request)
	{
		$bar = $this->service->findById( $request->bar );
		$user = $this->userService->findByEmail( $request->user );
		$this->service->giveAdmin($bar, $user);
		return response()->json(['user' =>  $user]);
	}

	/**
	 * destroyUser
	 * Add user in a account.
	 * @return Response
	 * @author Gustavo Henrique
	 */
	public function destroyUser(UserRequest $request)
	{
		$bar = $this->service->findById( $request->bar );
		$user = $this->userService->findByEmail( $request->user );
		$success = $this->service->removeUser($bar, $user);
		return response()->json(['success' =>  $success]);
	}

}
