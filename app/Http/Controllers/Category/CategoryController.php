<?php namespace nightcheers\Http\Controllers\Category;

use nightcheers\Http\Controllers\Controller;
use Illuminate\Http\Request;
use nightcheers\Http\Requests\Category\CategoryRequest;
use nightcheers\Category;
use nightcheers\Bar;
use Session;

class CategoryController extends Controller {

	/**
	 * index
	 * Show the application dashboard screen to the user.
	 * @return Response
	 */
	public function index()
	{
		$data = [
			'categories' => Category::where('bar_id', '=', Session::get('app.bar'))->get()
		];
		return view('pages.category.index')->with(array_merge($data, $this->basicData));
	}

	/**
	 * search
	 * Search a product
	 * @param  Request $request 
	 * @param  Int  $id      
	 * @return Array
	 * @author Gustavo Henrique
	 */
	public function search(Request $request, $id)
	{
		$data = [];

		$results = Category::where('bar_id', $id)->where('name', 'like', '%' . $request->q . '%')->get();

		foreach ($results as $result) {
			$data[] = ['result' => $result->name, 'id' => $result->id];
		}

		return response()->json($data);
	}

	/**
	 * create
	 * Show the application dashboard screen to the user.
	 * @return Response
	 */
	public function create()
	{
		$data = [
			'categories' => Category::where('bar_id', '=', Session::get('app.bar'))->get()
		];
		return view('pages.category.create')->with($data);
	}

	/**
	 * store
	 * Show the application dashboard screen to the user.
	 * @return Response
	 */
	public function store(CategoryRequest $request)
	{
		$category = new Category();
		$category->name = $request->input('name');
		$category->bar_id = Session::get('app.bar');
		if ( $request->input('category') != 0 ) {
			$category->category_id = $request->input('category');
		}
		$category->save();

		return redirect()->route('categories');
	}

	/**
	 * edit
	 * Edit a category
	 * @param  Int  $id
	 * @param  Illuminate\Http\Request $request
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function edit($id, Request $request)
	{
		$data = [
			'category' => Category::find($id),
			'categories' => Category::all()
		];

		return view('pages.category.edit')->with($data);
	}	

	/**
	 * update
	 * Update a category
	 * @param  Int  $id
	 * @param  Illuminate\Http\Request $request
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function update($id, CategoryRequest $request)
	{
		$category = Category::find($id);
		$category->name = $request->input('name');
		$category->bar_id = Session::get('app.bar');
		if ( $request->input('category') != 0 ) {
			$category->category_id = $request->input('category');
		} else {
			$category->category_id = null;
		}
		$category->save();

		return redirect()->route('categories');
	}	
	
	/**
	 * destroy
	 * Destroy a category
	 * @param  Int  $id
	 * @param  Illuminate\Http\Request $request
	 * @return View
	 * @author Gustavo Henrique
	 */
	public function destroy($id, Request $request)
	{
		Category::find($id)->delete();
		return redirect()->route('categories');
	}

}
