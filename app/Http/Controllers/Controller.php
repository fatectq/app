<?php namespace nightcheers\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use nightcheers\Bar;
use Session;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	protected $basicData;

	public function __construct()
	{

		$this->basicData = [
			'indexBar' => Bar::find(Session::get('app.bar'))
		];
	}

}
