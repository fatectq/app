<?php

Route::get('/order/create', [
	'as' => 'create_order',
	'uses' => 'Order\OrderController@create'
]);

Route::get('/order/today', [
	'as' => 'today_order',
	'uses' => 'Order\OrderController@today'
]);

Route::get('/order/search/{id}', [
	'as' => 'search_order',
	'uses' => 'Order\OrderController@search'
]);

Route::get('/orders', [
	'as' => 'orders',
	'uses' => 'Order\OrderController@index'
]);

Route::post('/order/create', [
	'as' => 'store_order',
	'uses' => 'Order\OrderController@store'
]);

Route::post('/order/{id}/edit', [
	'as' => 'update_order',
	'uses' => 'Order\OrderController@update'
]);

Route::post('/order/{id}/finish', [
	'as' => 'finishing_order',
	'uses' => 'Order\OrderController@finishing'
]);

Route::get('/order/{id}/finish', [
	'as' => 'finish_order',
	'uses' => 'Order\OrderController@finish'
]);

Route::get('/order/{id}/edit', [
	'as' => 'edit_order',
	'uses' => 'Order\OrderController@edit'
]);

Route::get('/order/{id}/view', [
	'as' => 'view_order',
	'uses' => 'Order\OrderController@view'
]);
