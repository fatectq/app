<?php

Route::get('/user/search/{id}', [
	'as' => 'search_user',
	'uses' => 'User\UserController@search'
]);

