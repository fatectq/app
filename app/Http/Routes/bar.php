<?php

Route::get('/bar', [
	'as' => 'create_bar',
	'uses' => 'Bar\BarController@create'
]);

Route::get('/bar/{id}/change', [
	'as' => 'change_bar',
	'uses' => 'Bar\BarController@change'
]);

Route::get('/bar/{id}/edit', [
	'as' => 'edit_bar',
	'uses' => 'Bar\BarController@edit'
]);

Route::get('/bar/{id}/delete', [
	'as' => 'destroy_bar',
	'uses' => 'Bar\BarController@destroy'
]);

Route::post('/bar/{id}edit', [
	'as' => 'update_bar',
	'uses' => 'Bar\BarController@update'
]);

Route::post('/bar/user/add', [
	'as' => 'add_user_bar',
	'uses' => 'Bar\BarController@addUser'
]);

Route::post('/bar/user/admin', [
	'as' => 'admin_user_bar',
	'uses' => 'Bar\BarController@giveAdmin'
]);

Route::post('/bar/user/destroy', [
	'as' => 'destroy_user_bar',
	'uses' => 'Bar\BarController@destroyUser'
]);

Route::post('/bar', [
	'as' => 'store_bar',
	'uses' => 'Bar\BarController@store'
]);

Route::get('/bars', [
	'as' => 'bars',
	'uses' => 'Bar\BarController@index'
]);