<?php

Route::get('/report/promotion', [
	'as' => 'report_promotion',
	'uses' => 'Report\ReportController@promotion'
]);

Route::get('/report/product', [
	'as' => 'report_product',
	'uses' => 'Report\ReportController@product'
]);

Route::get('/report/order', [
	'as' => 'report_order',
	'uses' => 'Report\ReportController@order'
]);

Route::get('/report/print', [
	'as' => 'report_print',
	'uses' => 'Report\ReportController@printer'
]);

Route::post('/report/promotion', [
	'as' => 'research_report_promotion',
	'uses' => 'Report\ReportController@researchPromotion'
]);

Route::post('/report/product', [
	'as' => 'research_report_user',
	'uses' => 'Report\ReportController@researchProduct'
]);

Route::post('/report/order', [
	'as' => 'research_report_order',
	'uses' => 'Report\ReportController@researchOrder'
]);

