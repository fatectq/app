<?php 

Route::get('/tables', [
	'as' => 'tables',
	'uses' => 'Table\TableController@index'
]);

Route::get('/tables/create', [
	'as' => 'create_tables',
	'uses' => 'Table\TableController@create'
]);

Route::post('/tables/create', [
	'as' => 'store_table',
	'uses' => 'Table\TableController@store'
]);

Route::get('/table/search/{id}', [
	'as' => 'search_table',
	'uses' => 'Table\TableController@search'
]);

Route::get('/tables/{id}/edit', [
	'as' => 'edit_table',
	'uses' => 'Table\TableController@edit'
]);

Route::get('/tables/{id}/delete', [
	'as' => 'destroy_table',
	'uses' => 'Table\TableController@destroy'
]);

Route::post('/tables/{id}/edit', [
	'as' => 'update_table',
	'uses' => 'Table\TableController@update'
]);