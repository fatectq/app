<?php

Route::get('/logout', [
	'as' => 'logout',
	'uses' => 'Auth\AuthController@logout'
]);

Route::group([/*'middleware' => 'guest'*/], function() {

	Route::get('/login', [
		'as' => 'login',
		'uses' => 'Auth\AuthController@login'
	]);

	Route::get('/register', [
		'as' => 'register',
		'uses' => 'Auth\AuthController@register'
	]);

	Route::post('/register', [
		'as' => 'singup',
		'uses' => 'Auth\AuthController@singup'
	]);

	Route::post('/login', [
		'as' => 'singin',
		'uses' => 'Auth\AuthController@singin'
	]);
	
});