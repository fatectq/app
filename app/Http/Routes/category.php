<?php

Route::get('/categories', [
	'as' => 'categories',
	'uses' => 'Category\CategoryController@index'
]);

Route::get('/category/create', [
	'as' => 'create_category',
	'uses' => 'Category\CategoryController@create'
]);

Route::get('/category/{id}/edit', [
	'as' => 'edit_category',
	'uses' => 'Category\CategoryController@edit'
]);

Route::get('/category/search/{id}', [
	'as' => 'search_category',
	'uses' => 'Category\CategoryController@search'
]);

Route::get('/category/{id}/delete', [
	'as' => 'destroy_category',
	'uses' => 'Category\CategoryController@destroy'
]);

Route::post('/category/{id}/edit', [
	'as' => 'update_category',
	'uses' => 'Category\CategoryController@update'
]);

Route::post('/category/create', [
	'as' => 'store_category',
	'uses' => 'Category\CategoryController@store'
]);