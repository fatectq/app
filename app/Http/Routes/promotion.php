<?php

Route::get('/promotions', [
    'as' => 'promotions',
    'uses' => 'Promotion\PromotionController@index'
]);

Route::get('/promotion/create', [
    'as' => 'create_promotion',
    'uses' => 'Promotion\PromotionController@create'
]);

Route::post('/promotion/create', [
	'as' => 'store_promotion',
	'uses' => 'Promotion\PromotionController@store'
]);

Route::get('/promotion/search/{id}', [
	'as' => 'search_promotion',
	'uses' => 'Promotion\PromotionController@search'
]);

Route::get('/promotion/{id}/edit', [
	'as' => 'edit_promotion',
	'uses' => 'Promotion\PromotionController@edit'
]);

Route::post('/promotion/{id}/edit', [
	'as' => 'edit_promotion',
	'uses' => 'Promotion\PromotionController@update'
]);

Route::get('/promotion/{id}/delete', [
	'as' => 'destroy_promotion',
	'uses' => 'Promotion\PromotionController@destroy'
]);
