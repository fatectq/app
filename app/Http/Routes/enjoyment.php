<?php

Route::get('/order/enjoyment/{token}', [
	'as' => 'enjoyment',
	'uses' => 'Enjoyment\EnjoymentController@index'
]);

Route::post('/order/enjoyment/{token}', [
	'as' => 'store_enjoyment',
	'uses' => 'Enjoyment\EnjoymentController@store'
]);

Route::post('/order/enjoyment/{token}/email', [
	'as' => 'add_email',
	'uses' => 'Enjoyment\EnjoymentController@email'
]);