<?php

Route::get('/products', [
	'as' => 'products',
	'uses' => 'Product\ProductController@index'
]);

Route::get('/product/create', [
	'as' => 'create_product',
	'uses' => 'Product\ProductController@create'
]);

Route::get('/product/{id}/edit', [
	'as' => 'edit_product',
	'uses' => 'Product\ProductController@edit'
]);

Route::get('/product/search/{id}', [
	'as' => 'search_product',
	'uses' => 'Product\ProductController@search'
]);

Route::post('/product/{id}/edit', [
	'as' => 'update_product',
	'uses' => 'Product\ProductController@update'
]);

Route::get('/product/{id}/delete', [
	'as' => 'destroy_product',
	'uses' => 'Product\ProductController@destroy'
]);

Route::post('/product/create', [
	'as' => 'store_product',
	'uses' => 'Product\ProductController@store'
]);