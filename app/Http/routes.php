<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/order/visit/{token}', [
	'as' => 'visit_order',
	'uses' => 'Order\OrderController@visit'
]);

require('Routes/enjoyment.php');

Route::group(['prefix' => 'auth'], function() {

	require('Routes/auth.php');
	
});

Route::group(['middleware' => 'auth'], function() {

	Route::group(['middleware' => 'only.bar'], function() {
		require('Routes/category.php');
		require('Routes/table.php');
		require('Routes/bar.php');
		require('Routes/product.php');
		require('Routes/promotion.php');
		require('Routes/user.php');
		require('Routes/order.php');
	});

	require('Routes/report.php');

	Route::get('/', [
		'middleware' => 'without.bar',
		'as' => 'dashboard',
		'uses' => 'Dashboard\DashboardController@index'
	]);

});