<?php namespace nightcheers\Http\Composers;

use Illuminate\Contracts\View\View;
use Session;
use nightcheers\Bar;
use Auth;
use nightcheers\Services\Bar\BarService;

class AppComposer {

  protected $barService;

  /**
   * Create a new profile composer.
   *
   * @param  UserRepository  $users
   * @return void
   */
  public function __construct(BarService $barService)
  {
    $this->barService = $barService;
  }

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    // dd($this->barService->getDefaultBar());
    $view->with('bars', Auth::user()->bars);
    $view->with('bar', $this->barService->getDefaultBar());
  }

}