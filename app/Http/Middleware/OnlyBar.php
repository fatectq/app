<?php namespace nightcheers\Http\Middleware;

use Closure;
use nightcheers\Services\User\UserService;
use nightcheers\Services\User\UserServiceInterface;
use Illuminate\Http\RedirectResponse;
use Auth;
use nightcheers\Bar;
use nightcheers\Table;
use nightcheers\Promotion;
use nightcheers\Category;
use nightcheers\Product;
use Session;
use URL;

class OnlyBar {

	protected $service;

	/**
	 * __construct
	 * Create a new filter instance.
	 * @param  economizee\Services\User\UserServiceInterface  $service
	 * @return void
	 * @author Gustavo Henrique
	 */
	public function __construct(UserServiceInterface $service)
	{
		$this->service = $service;
	}

	/**
	 * handle
	 * Handle an incoming request.
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 * @author Gustavo Henrique
	 */
	public function handle($request, Closure $next)
	{
		$url = explode('/', URL::current());

		if ( count($url) < 5  ) {
			return $next($request);
		} elseif ( ((int)$url[4]) == 0 ) {
			return $next($request);
		} else {

			$model;
			$bar = Bar::find(Session::get('app.bar'));

			$typeUrl = str_singular($url[3]);
			
			switch ($typeUrl) {
		    case "table":
	        $model = new Table();
	        break;
		    case "bar":
						return $next($request);
		    case "order":
						return $next($request);
		    case "product":
	        $model = new Product();
	        break;
		    case "promotion":
	        $model = new Promotion();
	        break;
		    case "category":
	        $model = new Category();
	        break;
	      default:
	      	dd('Tem que adiconar no Middleware OnlyBar');
			}

			if ( $bar->id == $model->find($url[4])->bar_id )
				return $next($request);

			return redirect()->route('dashboard');
		}

	}

}
