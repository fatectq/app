<?php namespace nightcheers\Http\Middleware;

use Closure;
use nightcheers\Services\User\UserService;
use nightcheers\Services\User\UserServiceInterface;
use Illuminate\Http\RedirectResponse;

class WithoutBar {

	protected $service;

	/**
	 * __construct
	 * Create a new filter instance.
	 * @param  economizee\Services\User\UserServiceInterface  $service
	 * @return void
	 * @author Gustavo Henrique
	 */
	public function __construct(UserServiceInterface $service)
	{
		$this->service = $service;
	}

	/**
	 * handle
	 * Handle an incoming request.
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 * @author Gustavo Henrique
	 */
	public function handle($request, Closure $next)
	{
		if ( $this->service->hasBar() ) {
			return $next($request);	
		}

		return redirect()->route('create_bar');
	}

}
