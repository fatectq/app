<?php

namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id', 'start_date', 'end_date'];

    public function bar() {
        return $this->belongsTo('nightcheers\Bar');
    }
    
    public function client() {
        return $this->belongsTo('nightcheers\Client');
    }

}
