<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * table
	 * Return the categories associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function items()
	{
		return $this->hasMany('nightcheers\Item', 'order_id');
	}

	/**
	 * table
	 * Return the categories associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function table()
	{
		return $this->belongsTo('nightcheers\Table');
	}

	/**
	 * client
	 * Return the categories associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function client()
	{
		return $this->belongsTo('nightcheers\Client');
	}

	/**
	 * client
	 * Return the categories associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function enjoyment()
	{
		return $this->hasOne('nightcheers\Enjoyment', 'order_id');
	}

}
