<?php namespace nightcheers;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'purchase_value', 'sale_value', 'category_id'];


	public function category()
  {
      return $this->belongsTo('nightcheers\Category');
  }

  /**
	 * items
	 * Return the items associated with account
	 * @return Illuminate\Support\Collection
	 * @author Gustavo Henrique
	 * 
	 */
	public function items()
	{
		return $this->hasMany('nightcheers\Item', 'product_id');
	}

}
