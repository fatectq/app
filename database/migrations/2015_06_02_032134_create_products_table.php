<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('category_id')->unsigned();
      $table->integer('bar_id')->unsigned();
      $table->string('name');
      $table->decimal('purchase_value', 10, 2);
      $table->decimal('sale_value', 10, 2);
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('products');
  }
}
