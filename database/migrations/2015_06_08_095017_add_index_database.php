<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexDatabase extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users_has_bar', function(Blueprint $table)
    {
      $table->index('user_id');
      $table->foreign('user_id')->references('id')->on('users');
      $table->index('bar_id');
      $table->foreign('bar_id')->references('id')->on('bars');
    });

    Schema::table('tables', function(Blueprint $table)
    {
      $table->index('bar_id');
      $table->foreign('bar_id')->references('id')->on('bars');
    });

    Schema::table('clients', function(Blueprint $table)
    {
      $table->index('bar_id');
      $table->foreign('bar_id')->references('id')->on('bars');
    });

    Schema::table('promotions', function(Blueprint $table)
    {
      $table->index('bar_id');
      $table->foreign('bar_id')->references('id')->on('bars');
      $table->index('client_id');
      $table->foreign('client_id')->references('id')->on('clients');
    });

    Schema::table('orders', function(Blueprint $table)
    {
      $table->index('table_id');
      $table->foreign('table_id')->references('id')->on('tables');
      $table->index('client_id');
      $table->foreign('client_id')->references('id')->on('clients');
    });

    Schema::table('items', function(Blueprint $table)
    {
      $table->index('order_id');
      $table->foreign('order_id')->references('id')->on('orders');
      $table->index('product_id');
      $table->foreign('product_id')->references('id')->on('products');
    });

    Schema::table('products', function(Blueprint $table)
    {
      $table->index('category_id');
      $table->foreign('category_id')->references('id')->on('categories');
      $table->index('bar_id');
      $table->foreign('bar_id')->references('id')->on('bars');
    });

    Schema::table('categories', function(Blueprint $table)
    {
      $table->index('category_id');
      $table->foreign('category_id')->references('id')->on('categories');
    });

    Schema::table('enjoyment', function(Blueprint $table)
    {
      $table->index('client_id');
      $table->foreign('client_id')->references('id')->on('clients');
      $table->index('order_id');
      $table->foreign('order_id')->references('id')->on('orders');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
     
  }
}
