<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnjoymentTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('enjoyment', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('client_id')->unsigned();
      $table->integer('order_id')->unsigned();
      $table->integer('note');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('enjoyment');
  }
}
