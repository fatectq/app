<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('promotions', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('bar_id')->unsigned();
      $table->integer('client_id')->unsigned();
      $table->dateTime('start_date');
      $table->dateTime('end_date');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('promotions');
  }
}
