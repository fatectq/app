<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('tables', function(Blueprint $table)
    {
      $table->increments('id');
      $table->integer('bar_id')->unsigned();
      $table->integer('seats')->unsigned();
      $table->string('name', 45);
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('tables');
  }
}
