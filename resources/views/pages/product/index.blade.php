@extends('layouts.normal.layout')

@section('title')
	Produtos
@stop

@section('content-header-title')
	Produtos
@stop

@section('script-script')
<script src="/js/app/common.js" type="text/javascript"></script>
@stop

@section('content-header-extra')
	<a href="{{ route('create_product') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Criar produto</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
		<!-- search form -->
    <div class="input-group col-xs-12">
      <input name="product" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="product_edit" data-url-search="{{ route('search_product', ['id' => $indexBar->id]) }}?q=%QUERY">
    </div>
	</div>
</div>

<hr />

<div class="row">
	<div class="col-lg-12 col-xs-12">
		<div class="table-responsive no-padding">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th>Nome</th>
						<th>Categoria</th>
						<th>Compra</th>
						<th>Venda</th>
						<th></th>
					</tr>
                                        @if(count($products) != 0) 
					@foreach($products as $product)
					<tr>
						<td class="name">{{ $product->name }}</td>
						<td class="category">{{ $product->category->name }}</td>
						<td class="purchase">R$ {{  number_format($product->purchase_value, 2, ',', '.') }}</td>
						<td class="sale">R$ {{ number_format($product->sale_value, 2, ',', '.') }}</td>
						<td>
							<div class="btn-group" style="float:right;">
								<a href="{{ route('edit_product', ['id' => $product->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
								<a href="{{ route('destroy_product', ['id' => $product->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-trash-o"></i></a>
							</div>
						</td>
					</tr>
					@endforeach
                                        @else
                                        <tr>
                                            <td colspan="4"><span style="color: red;">{{ trans('messages.search.no_result') }}</span></td>
                                        </tr>
                                        @endif
				</tbody>
			</table>
		</div>
		{!! $products->render() !!}
	</div>
</div><!-- /.row (main row) -->

@stop