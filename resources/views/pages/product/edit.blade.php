@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Editar Produto
@stop

@section('content-header-extra')
<a href="{{ route('products') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
    <div class="row">
      <div class="col-lg-6 col-xs-12">
		  	<form method="post" action="{{ route('update_product', ['id' => $product->id]) }}">

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
		        <label>Nome</label>
		        <input name="name" type="text" class="form-control" value="{{ $product->name }}" placeholder="Nome">
		        <span class="help-block text-error">{{ $errors->first('name') }}</span>
		      </div>

		  		<div class="form-group {{ $errors->has('sale_value') ? 'has-error' : '' }}">
		        <label>Valor de venda</label>
		        <input name="sale_value" type="text" class="form-control money" value="{{ $product->sale_value }}" placeholder="Valor de venda">
		        <span class="help-block text-error">{{ $errors->first('sale_value') }}</span>
		      </div>

		  		<div class="form-group {{ $errors->has('purchase_value') ? 'has-error' : '' }}">
		        <label>Valor de compra</label>
		        <input name="purchase_value" type="text" class="form-control money" value="{{ $product->purchase_value }}" placeholder="Valor de compra">
		        <span class="help-block text-error">{{ $errors->first('purchase_value') }}</span>
		      </div>

		  		<div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
		        <label>Categoria</label>
		        <select class="form-control" name="category">
	        		<option value="">Selecione...</option>
	        		@foreach ($categories as $category)
		        		<option value="{{ $category->id }}" @if($category->id == $product->category_id) selected @endif>{{ $category->name }}</option>
		        	@endforeach
		        </select>
		        <span class="help-block text-error">{{ $errors->first('category') }}</span>
		      </div>

		      <button type="submit" class="btn btn-primary">Salvar</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop