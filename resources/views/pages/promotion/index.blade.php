@extends('layouts.normal.layout')
@section('title')
Promoções
@stop

@section('content-header-title')
Promoções
@stop

@section('script-script')
<script src="/js/app/common.js" type="text/javascript"></script>
@stop

@section('content-header-extra')
<a href="{{ route('create_promotion') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Criar promoção</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
    <div class="col-lg-12 col-xs-12">
        <!-- search form -->
        <div class="input-group col-xs-12">
            <input name="promotion" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="promotion_edit" data-url-search="{{ route('search_promotion', ['id' => $indexBar->id]) }}?q=%QUERY">
        </div>
    </div>
</div>

<hr />

<div class="row">
    <div class="col-lg-12 col-xs-12">
        <div class="table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <th>Bar</th>
                        <th>Cliente</th>
                        <th>Data Inicial</th>
                        <th>Data Final</th>
                        <th></th>
                    </tr>
                    
                    @if(count($promotions) != 0) 
                    @foreach($promotions as $promotion)
                    <tr>
                        <td class="bar-user">{{ $promotion->bar->name }}</td>
                        <td class="client">{{ $promotion->client->name }}</td>
                        <td class="start-date">{{implode("/",array_reverse(explode("-",substr($promotion->start_date,0, 10 ))))}}</td>
                        <td class="end-date">{{ implode("/",array_reverse(explode("-",substr($promotion->end_date ,0, 10 ))))}}</td>
                        <td>
                        <div class="btn-group" style="float:right;">
                            <a href="{{ route('edit_promotion', ['id' => $promotion->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('destroy_promotion', ['id' => $promotion->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-trash-o"></i></a>
                        </div>
                        </td>
                    </tr>
                    @endforeach 
                    @else
                    <tr>
                        <td colspan="4"><span style="color: red;">{{ trans('messages.search.no_result') }}</span></td>
                    </tr>
                    @endif
                    
                </tbody>
            </table>
        </div>
    </div>
</div><!-- /.row (main row) -->

@stop