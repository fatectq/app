@extends('layouts.normal.layout')

@section('script-script')
<script src="/js/app/promotionCreate.js" type="text/javascript"></script>
@stop

@section('title')
Promoções
@stop

@section('content-header-title')
Criar Promoção
@stop

@section('content-header-extra')
<a href="{{ route('promotions') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')
<!-- Main row -->
<div class="row">
    <div class="col-lg-12 col-xs-12">
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <form method="post" action="{{ route('store_promotion') }}">
                    <input type="hidden" id="hddClientId" value="{{!is_null($promotion)?$promotion->client->id : ''}}" />
                    <div class="form-group {{ $errors->has('client') ? 'has-error' : '' }}">
                        <label>Cliente</label>
                        <select class="form-control" name="client" id="client">
                            <option value="">Selecione...</option> 
                            @foreach ($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                            @endforeach
                        </select>
                        <span class="help-block text-error">{{ $errors->first('client') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                        <label>Data Inicial</label>
                        <input name="start_date" type="text" class="form-control date" placeholder="{{ trans('messages.promotion.star_date') }}">
                        <span class="help-block text-error">{{ $errors->first('start_date') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                        <label>Data Final</label>
                        <input name="end_date" type="text" class="form-control date" placeholder="{{ trans('messages.promotion.end_date') }}">
                        <span class="help-block text-error">{{ $errors->first('end_date') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                        <label>Desconto</label>
                        <input name="discount" type="text" class="form-control" placeholder="Desconto">
                        <span class="help-block text-error">{{ $errors->first('discount') }}</span>
                    </div>

                    <button type="submit" class="btn btn-primary">{{ trans('messages.common.create_button') }}</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
    </div>
</div><!-- /.row (main row) -->

@stop