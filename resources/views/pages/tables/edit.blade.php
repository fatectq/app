@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Editar Mesa
@stop

@section('content-header-extra')
<a href="{{ route('tables') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
    <div class="row">
      <div class="col-xs-5">
		  	<form method="post" action="{{ route('update_table', ['id' => $tables->id]) }}">

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
		        <label>Nome</label>
		        <input name="name" type="text" class="form-control" value="{{ $tables->name }}" placeholder="Nome">
		        <span class="help-block text-error">{{ $errors->first('name') }}</span>
		      </div>

					<div class="form-group {{ $errors->has('seats') ? 'has-error' : '' }}">
		        <label>Assentos</label>
		        <input name="seats" type="number" class="form-control" value="{{ $tables->seats }}" placeholder="Assentos">
		        <span class="help-block text-error">{{ $errors->first('seats') }}</span>
		      </div>		  		

		      <button type="submit" class="btn btn-primary">Salvar</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop