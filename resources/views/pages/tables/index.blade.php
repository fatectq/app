@extends('layouts.normal.layout')

@section('title')
	Mesas
@stop

@section('content-header-title')
	Mesas
@stop

@section('script-script')
<script src="/js/app/common.js" type="text/javascript"></script>
@stop

@section('content-header-extra')
	<a href="{{ route('create_tables') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Criar mesa</a>
@stop

@section('content')
<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
		<!-- search form -->
    <div class="input-group col-xs-12">
      <input name="product" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="table_edit" data-url-search="{{ route('search_table', ['id' => $indexBar->id]) }}?q=%QUERY">
    </div>
	</div>
</div>

<hr />
<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
		<div class="table-responsive no-padding">
			<table class="table">
				<tbody>
					<th>Nome</th>
					<th>Assentos</th>
					<th></th>
                                        @if(count($tables) != 0) 
					@foreach($tables as $table)
					<tr class="account">
						<td class="name">{{ $table->name }}</td>
						<td class="seats">{{ $table->seats }}</td>
						<td>
							<div class="btn-group" style="float:right;">
								<a href="{{ route('edit_table', ['id' => $table->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
								<a href="{{ route('destroy_table', ['id' => $table->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-trash-o"></i></a>
							</div>
						</td>
					</tr>
					@endforeach
                                        @else
                                        <tr>
                                            <td colspan="4"><span style="color: red;">{{ trans('messages.search.no_result') }}</span></td>
                                        </tr>
                                        @endif
				</tbody>
			</table>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop