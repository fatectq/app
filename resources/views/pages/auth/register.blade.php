@extends('layouts.auth.layout')

@section('title')
  {{ trans('messages.auth.new_user') }}
@stop

@section('header')
  {{ trans('messages.auth.new_user') }}
@stop

@section('content')
  <form action="{{ route('singup') }}" method="post">
    <div class="body bg-gray">
      <div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
        <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="{{ trans('messages.auth.name_placeholder') }}" autofocus />
        <span class="help-block text-error ">{{{ $errors->first('name') }}}</span>
      </div>
      <div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
        <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ trans('messages.auth.email_placeholder') }}" />
        <span class="help-block text-error ">{{{ $errors->first('email') }}}</span>
      </div>
      <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
        <input type="password" name="password" class="form-control" placeholder="{{ trans('messages.auth.password_placeholder') }}" />
        <span class="help-block text-error ">{{{ $errors->first('password') }}}</span>
      </div>
      <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
        <input type="password" name="password_confirmation" class="form-control" placeholder="{{ trans('messages.auth.password_again_placeholder') }}" />
      </div>
    </div>
    <div class="footer">
      <button type="submit" class="btn bg-light-blue btn-block">{{ trans('messages.auth.create') }}</button>

      <a href="{{ route('login') }}" class="text-center">{{ trans('messages.auth.login') }}</a>
      
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
  </form>
@stop
     