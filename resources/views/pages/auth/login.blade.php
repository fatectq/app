@extends('layouts.auth.layout')

@section('title')
  {{ trans('messages.auth.login') }}
@stop

@section('header')
  {{ trans('messages.auth.login') }}
@stop

@section('content')
  <form action="{{ route('singin') }}" method="post">
    <div class="body bg-gray">

      @if (Session::has('success_singup'))
        <div class="alert alert-success alert-dismissable" style="margin-left: 0 !important;">
          {{ Session::get('success_singup') }}
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
      @endif

      @if (Session::has('success_confirm'))
        <div class="alert alert-success alert-dismissable" style="margin-left: 0 !important;">
          {{ Session::get('success_confirm') }}
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
      @endif

      @if (Session::has('error_confirm'))
        <div class="alert alert-danger alert-dismissable" style="margin-left: 0 !important;">
          {{ Session::get('error_confirm') }}
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
      @endif

      <div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
        <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ trans('messages.auth.email_placeholder') }}" autofocus />
        <span class="help-block text-error ">{{{ $errors->first('email') }}}</span>
      </div>
      <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
        <input type="password" name="password" class="form-control" placeholder="{{ trans('messages.auth.password_placeholder') }}" />
        <span class="help-block text-error ">{{{ $errors->first('password') }}}</span>
      </div>
      <div class="form-group">
        <input type="checkbox" name="remember"/> {{ trans('messages.auth.keep_me_conected') }}
      </div>
    </div>
    <div class="footer">
      <button type="submit" class="btn bg-light-blue btn-block">{{ trans('messages.auth.login') }}</button>

      <a href="{{ route('register') }}" class="text-center">{{ trans('messages.auth.register') }}</a>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>
@stop
     