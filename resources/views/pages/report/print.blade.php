<!DOCTYPE html>
<html>
  <head>
    <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/fontawesome/font-awesome.min.css" rel="stylesheet" type="text/css" />
  </head>
  <body>

    @if( $type == 'product' )
      @include('elements.print.product')
    @elseif( $type == 'promotion' )
      @include('elements.print.promotion')
    @else
      @include('elements.print.order')
    @endif

    <script  type="text/javascript">
      window.print();
    </script>
  </body>
</html>