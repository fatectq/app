@extends('layouts.normal.layout')

@section('script-script')
<script src="/js/app/bar.js" type="text/javascript"></script>
@stop

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Relatório de pedido
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
	  	<form method="post" action="{{ route('research_report_order') }}">

  			<div class="row">
      		<div class="col-xs-3">
			  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
			        <label>Data min</label>
			        <input class="form-control date" data-inputmask="'alias':'dd/mm/yyyy'" data-mask name="date-min">
			      </div>
					</div>

      		<div class="col-xs-3">
			  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
			        <label>Data máx</label>
			        <input class="form-control date" data-inputmask="'alias':'dd/mm/yyyy'" data-mask name="date-max">
			      </div>
					</div>

      		<div class="col-xs-3">
			  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
			        <label>Valor min</label>
			        <input class="form-control" type="text" name="value-min">
			      </div>
					</div>

      		<div class="col-xs-3">
			  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
			        <label>Valor máx</label>
			        <input class="form-control" type="text" name="value-max">
			      </div>
					</div>
        </div>

    		<div class="row">
					<div class="col-xs-12 box-footer">
			      <button type="submit" class="btn btn-primary">Gerar relatório</button>
			      <input type="hidden" name="_token" value="{{ csrf_token() }}">
			      <a href="{{ $printUrl }}" target="_blank" class="btn btn-primary" @if( count($orders) == 0 ) disabled @endif><i class="fa fa-print"></i> Imprimir</a>
          </div>
        </div>

		  	</form>
		</div>
</div><!-- /.row (main row) -->

<hr />

<div class="row">
	<div class="col-lg-12 col-xs-12">

  	<div class="table-responsive no-padding">
      <table class="table">
        <thead>
        	<tr>
            <th>Total</th>
            <th>Data do pedido</th>
            <th>Qtd. de itens</th>
          </tr>
        </thead>
        <tbody>
        	@foreach($orders as $order)
        	<tr>
            <td>{{ $order->total }}</td>
            <td>{{ $order->created_at }}</td>
            <td>{{ $order->items->count() }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
	  </div>

	</div>
</div>

@stop