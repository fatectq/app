@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Relatório de promoções
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
	  	<form method="post" action="{{ route('research_report_promotion') }}">

  			<div class="row">
      		<div class="col-xs-6">
			  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
			        <label>Data min</label>
			        <input class="form-control date" name="date-min" data-inputmask="'alias':'dd/mm/yyyy'" data-mask>
			        <span class="help-block text-error">{{{ $errors->first('name') }}}</span>
			      </div>
					</div>

      		<div class="col-xs-6">
			  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
			        <label>Data máx</label>
			        <input class="form-control date" name="date-max" data-inputmask="'alias':'dd/mm/yyyy'" data-mask>
			        <span class="help-block text-error">{{{ $errors->first('name') }}}</span>
			      </div>
					</div>
				</div>

    		<div class="row">
					<div class="col-xs-12 box-footer">
			      <button type="submit" class="btn btn-primary">Gerar relatório</button>
			      <input type="hidden" name="_token" value="{{ csrf_token() }}">
			      <a href="{{ $printUrl }}" target="_blank" class="btn btn-primary" @if( count($promotions) == 0 ) disabled @endif><i class="fa fa-print"></i> Imprimir</a>
          </div>
        </div>

		  	</form>
		</div>
	</div>


<hr />

<div class="row">
	<div class="col-lg-12 col-xs-12">

  	<div class="table-responsive no-padding">
      <table class="table">
        <thead>
        	<tr>
            	<th>Data de Inicio</th>
            	<th>Data de Término</th>
            	<th>Cliente</th>
            	<th>Desconto</th>
          	</tr>
        </thead>
        <tbody>
        	@foreach($promotions as $promotion)
        	<tr>
            	<td>{{ $promotion->start_date }}</td>
            	<td>{{ $promotion->end_date }}</td>
            	<td>{{ $promotion->client->name }}</td>
            	<td>{{ $promotion->discount }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
	  </div>

	</div>
</div>

</div><!-- /.row (main row) -->
@stop