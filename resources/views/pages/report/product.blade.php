@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Relatório de produto
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
  	<form method="post" action="{{ route('research_report_user') }}">

			<div class="row">
    		<div class="col-xs-2">
		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Data cadastro min</label>
		        <input class="form-control date" name="date-min" data-inputmask="'alias':'dd/mm/yyyy'" data-mask>
		      </div>
				</div>

    		<div class="col-xs-2">
		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Data cadastro máx</label>
		        <input class="form-control date" name="date-max" data-inputmask="'alias':'dd/mm/yyyy'" data-mask>
		      </div>
				</div>

    		<div class="col-xs-2">
		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Valor de venda min</label>
		        <input class="form-control" type="text" name="value-sale-min">
		      </div>
				</div>

    		<div class="col-xs-2">
		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Valor de venda máx</label>
		        <input class="form-control" type="text" name="value-sale-max">
		      </div>
				</div>

    		<div class="col-xs-2">
		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Valor de compra min</label>
		        <input class="form-control" type="text" name="value-purchase-min">
		      </div>
				</div>

    		<div class="col-xs-2">
		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Valor de venda máx</label>
		        <input class="form-control" type="text" name="value-purchase-max">
		      </div>
				</div>
      </div>

  		<div class="row">
				<div class="col-xs-12 box-footer">
		      <button type="submit" class="btn btn-primary"><i class="fa fa-file-text-o"></i> Gerar relatório</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  		<a href="{{ $printUrl }}" target="_blank" class="btn btn-primary" @if( count($products) == 0 ) disabled @endif><i class="fa fa-print"></i> Imprimir</a>
        </div>
      </div>

	  	</form>
	</div>
</div>

<hr />

<div class="row">
	<div class="col-lg-12 col-xs-12">

  	<div class="table-responsive no-padding">
      <table class="table">
        <thead>
        	<tr>
            <th>Produto</th>
            <th>Preço de compra</th>
            <th>Preço de venda</th>
            <th>Qtd. vendida</th>
          </tr>
        </thead>
        <tbody>
        	@foreach($products as $product)
        	<tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->purchase_value }}</td>
            <td>{{ $product->sale_value }}</td>
            <td>{{ $product->items->count() }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
	  </div>

	</div>
</div>

@stop