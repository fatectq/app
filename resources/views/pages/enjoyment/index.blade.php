@extends('layouts.auth.layout')

@section('title')
  Pedido #{{ $order->id }}
@stop

@section('header')
  Olá, {{ $order->client->name }}
@stop

@section('content')
<div class="body bg-white">
  <div class="item" style="text-align: center;">
    @if( is_null($order->enjoyment) )
      Por favor avalie o atendimento, comida e estabelecimento: <b>{{ $order->table->bar->name }}</b>

      <br /><br />

      <form method="post" action="{{ route('store_enjoyment', ['token' => $order->token]) }}">
        <table class="table">
          <tr>
            <td>
              <label for="note-1">1</label>
            </td>
            <td>
              <label for="note-2">2</label>
            </td>
            <td>
              <label for="note-2">3</label>
            </td>
            <td>
              <label for="note-2">4</label>
            </td>
            <td>
              <label for="note-2">5</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" class="minimal-blue" name="note" value="1" id="note-1">
            </td>
            <td>
              <input type="radio" class="minimal-blue" name="note" value="2" id="note-2">
            </td>
            <td>
              <input type="radio" class="minimal-blue" name="note" value="3" id="note-3">
            </td>
            <td>
              <input type="radio" class="minimal-blue" name="note" value="4" id="note-4">
            </td>
            <td>
              <input type="radio" class="minimal-blue" name="note" value="5" id="note-5">
            </td>
          </tr>
        </table>
        <button type="submit" class="btn btn-block btn-success">Avaliar</button>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form>
    @else
      <div class="alert alert-success alert-dismissable" style="margin-left: 0 !important;">
        Obrigado por avaliar o estabelecimento!
      </div>
      <a href="{{ route('visit_order', ['token' => $order->token]) }}" class="btn btn-success">
        <i class="fa fa-arrow-left"></i> Ver comanda
      </a>
    @endif

  </div>
</div>
@stop
     