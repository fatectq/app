@extends('layouts.clean.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Criar Estabelecimento
@stop

@section('content-header-extra')
<a href="{{ route('bars') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Estabelecimentos</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
    <div class="row">
      <div class="col-xs-5">
		  	<form method="post" action="{{ route('store_bar') }}">

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}}">
		        <label>Nome</label>
		        <input name="name" type="text" class="form-control" value="{{ old('name') }}" placeholder="Nome">
		        <span class="help-block text-error">{{{ $errors->first('name') }}}</span>
		      </div>

		      <button type="submit" class="btn btn-primary">Criar</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop