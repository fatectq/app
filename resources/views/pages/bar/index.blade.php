@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Estabelecimentos
@stop

@section('content-header-extra')
	<a href="{{ route('create_bar') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Criar estabelecimento</a>
@stop

@section('content')

<div class="row">
	<div class="col-lg-12 col-xs-12">
		<div class="table-responsive no-padding">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th>Nome</th>
						<th></th>
					</tr>
					@foreach($indexBars as $indexBar)
					<tr>
						<td class="name">{{ $indexBar->name }}</td>
						<td>
							@if( $indexBar->isAdmin() )
							<div class="btn-group" style="float:right;">
								<a href="{{ route('edit_bar', ['id' => $indexBar->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
								<a href="{{ route('destroy_bar', ['id' => $indexBar->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-trash-o"></i></a>
							</div>
							@endif
						</td>
					</tr>
					@endforeach

				</tbody>
			</table>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop