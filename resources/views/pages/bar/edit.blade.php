@extends('layouts.normal.layout')

@section('script-script')
<script src="/js/app/bar.js" type="text/javascript"></script>
@stop

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Editar Estabelecimento: {{ $editBar->name }}
@stop

@section('content-header-extra')
	<a href="{{ route('bars') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Estabelecimentos</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">

		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
			  <li class="active"><a href="#data" data-toggle="tab">Dados</a></li>
			  <li><a href="#users" data-toggle="tab">Usários</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="data">
			    <div class="row">
			      <div class="col-xs-6">
							@include('elements.pages.bar.tab_data')
				  	</div>
					</div>
				</div>

				<div class="tab-pane" id="users">
			    <div class="row">
			      <div class="col-xs-12">
							@include('elements.pages.bar.tab_users')
				  	</div>
					</div>
				</div>

	  	</div>
	  </div>
	</div>
</div><!-- /.row (main row) -->

@stop