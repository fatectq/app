@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Categorias 
@stop

@section('script-script')
<script src="/js/app/common.js" type="text/javascript"></script>
@stop

@section('content-header-extra')
	<a href="{{ route('create_category') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Criar categoria</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
		<!-- search form -->
    <div class="input-group col-xs-12">
      <input name="product" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="category_edit" data-url-search="{{ route('search_category', ['id' => $indexBar->id]) }}?q=%QUERY">
    </div>
	</div>
</div>

<hr />

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
		<div class="table-responsive no-padding">
			<table class="table">
				<tbody>
                                        @if(count($categories) != 0) 
					@foreach($categories as $category)
					<tr class="account">
						<td class="name">{{ $category->name }}</td>
						<td>
							<div class="btn-group" style="float:right;">
								<a href="{{ route('edit_category', ['id' => $category->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
								<a href="{{ route('destroy_category', ['id' => $category->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-trash-o"></i></a>
							</div>
						</td>
					</tr>
					@endforeach
                                        @else
                                        <tr>
                                            <td colspan="4"><span style="color: red;">{{ trans('messages.search.no_result') }}</span></td>
                                        </tr>
                                        @endif
				</tbody>
			</table>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop