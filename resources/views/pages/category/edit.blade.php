@extends('layouts.normal.layout')

@section('title')
	Dashboard
@stop

@section('content-header-title')
	Editar categoria
@stop

@section('content-header-extra')
<a href="{{ route('categories') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
    <div class="row">
      <div class="col-xs-5">
		  	<form method="post" action="{{ route('update_category', ['id' => $category->id]) }}">

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
		        <label>Nome</label>
		        <input name="name" type="text" class="form-control" value="{{ $category->name }}" placeholder="Nome">
		        <span class="help-block text-error">{{ $errors->first('name') }}</span>
		      </div>

		  		<div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
		        <label>Categoria pai</label>
		        <select class="form-control" name="category">
		        		<option value="">Selecione..</option>
		        	@foreach($categories as $cat)
		        		<option value="{{ $cat->id }}" @if($cat->id == $category->category_id) selected @endif>
		        			{{ $cat->name }}
		        		</option>
		        	@endforeach
		        </select>
		        <p class="help-block {{ $errors->has('name') ? 'text-error' : '' }}"></p>
		      </div>

		      <button type="submit" class="btn btn-primary">Salvar</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop