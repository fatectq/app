@extends('layouts.normal.layout')

@section('script-plugin')
	<!-- chart.js charts -->
<script src="/js/plugins/chartjs/Chart.min.js" type="text/javascript"></script>

<script type="text/javascript">
// Get context with jQuery - using jQuery's .get() method.
var ctx = $("#chart").get(0).getContext("2d");

var data = {
  labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
  datasets: [
    {
      label: "Resumo anual",
      fillColor: "rgba(0, 68, 102,0.5)",
      strokeColor: "rgba(220,220,220,1)",
      pointColor: "rgba(68,68,68,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(220,220,220,1)",
      data: [
      	"{{ $graph[1] }}", 
      	"{{ $graph[2] }}", 
      	"{{ $graph[3] }}", 
      	"{{ $graph[4] }}", 
      	"{{ $graph[5] }}", 
      	"{{ $graph[6] }}", 
      	"{{ $graph[7] }}",
      	"{{ $graph[8] }}",
      	"{{ $graph[9] }}",
      	"{{ $graph[10] }}",
      	"{{ $graph[11] }}",
      	"{{ $graph[12] }}"
      ]
    }
  ]
};

// This will get the first returned node in the jQuery collection.
var myNewChart = new Chart(ctx).Line(data);
</script>

@stop

@section('script-script')
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/js/AdminLTE/dashboard.js" type="text/javascript"></script>
@stop

@section('title')
	Dashboard
@stop

@section('content-header-title')
	{{ trans('messages.dashboard.title') }}
@stop

@section('content')

<!-- Small boxes (Status box) -->
<div class="row">
  @include('elements.dashboard.status')
</div><!-- /.row -->

<!-- Main row -->
<div class="row">
  @include('elements.dashboard.main')
</div><!-- /.row (main row) -->

@stop