@extends('layouts.normal.layout')

@section('script-script')
<script src="/js/app/account.js" type="text/javascript"></script>
@stop

@section('title')
	Dashboard
@stop

@section('content-header-title')
	{{ trans('messages.account.edit_header', ['name' => $editAccount->name]) }}
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-6">
    <div class="row">

      <div class="col-xs-5">
		  	<form method="post" action="{{ route('update_account', ['token' => $editAccount->token]) }}">

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
		        <label>{{ trans('messages.account.name') }}</label>
		        <input name="name" type="text" class="form-control" value="{{ $editAccount->name }}" placeholder="{{ trans('messages.account.name') }}">
		        <p class="help-block {{ $errors->has('name') ? 'text-error' : '' }}}">{{ trans('messages.account.name_help') }}</p>
		        <span class="help-block text-error">{{{ $errors->first('name') }}}</span>
		      </div>

		      <button type="submit" class="btn btn-primary">{{ trans('messages.common.save_button') }}</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>

      <div class="col-xs-7">
      	<label>{{ trans('messages.account.users') }}</label>
      	<div class="table-responsive no-padding bg-white">
					<table class="table">
						<tbody id="users-account">
				  		@foreach($usersAccount as $accountUser)
								<tr class="account-user" id="{{ $accountUser->id }}">
									<td class="name">{{ $accountUser->name }}</td>
									<td>
										<div class="btn-group" style="float:right;">
											<a href="#" class="btn btn-sm btn-default"><i class="fa fa-envelope"></i></a>
											<a href="#" class="btn btn-sm btn-default remove-user" data-email="{{ $accountUser->email }}" data-user="{{ $accountUser->id }}"><i class="fa fa-trash-o"></i></a>
										</div>
									</td>
								</tr>
				  		@endforeach
						</tbody>
					</table>
				</div>

				<div class="alert alert-danger alert-dismissable" id="error-remove-user" style="margin-left: 0 !important; display: none;">
          {{ trans('messages.account.error_remove_user') }}
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>

				<hr />

				<form method="post" id="form-add-user-account">

		  		<div class="form-group {{ $errors->has('user') ? 'has-error' : '' }}}">
		        <label>{{ trans('messages.account.add_user') }}</label>
			  		<div class="input-group">
			        <input name="user" type="text" id="input-user" class="form-control typeahead" placeholder="{{ trans('messages.account.add_user_input') }}" data-url-search="{{ route('search_user', ['token' => $editAccount->token]) }}?q=%QUERY">
			        <span class="input-group-btn">
			      		<button type="submit" class="btn btn-primary" id="add-user">{{ trans('messages.common.add_button') }}</button>
			        </span>
			      </div>
		        <span class="help-block text-error">{{{ $errors->first('user') }}}</span>
		      </div>

		      <input type="hidden" name="account" id="account-token" value="{{ $editAccount->token }}">
		      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
		  	</form>

		  	<div class="alert alert-danger alert-dismissable" id="error-add-user" style="margin-left: 0 !important; display: none;">
          {{ trans('messages.account.error_add_user') }}
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>

			</div>

		</div>
	</div>
</div><!-- /.row (main row) -->

@stop