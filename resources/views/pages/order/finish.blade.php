@extends('layouts.normal.layout')

@section('title')
	Editar Pedido
@stop

@section('content-header-title')
	Editar Pedido
@stop

@section('script-script')
<script src="/js/app/order.js" type="text/javascript"></script>
@stop

@section('content-header-extra')
<a href="{{ route('orders') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
    <div class="row">
      <div class="col-lg-6 col-xs-12">
		  	<form method="post" action="@if( $finished )  {{ route('finishing_order', ['id' => $order->id]) }} @else {{ route('update_order', ['id' => $order->id]) }} @endif">

		  		<div class="form-group">
		        <label>Nome</label>
		        <input name="name" type="text" class="form-control" value="{{ $order->client->name }}" disabled>
		      </div>

		  		<div class="form-group">
		        <label>Token</label>
		        <input name="name" type="text" class="form-control" value="{{ $order->token }}" disabled>
		      </div>

		  		<div class="form-group">
		        <label>Mesa</label>
		        <input name="name" type="text" class="form-control" value="{{ $order->table->name }}" disabled>
		      </div>

			    <div class="row">
			      <div class="col-lg-12 col-xs-12">
					  		<div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
					        <label>Produto</label>
					        <div class="input-group col-xs-12">
						        <input name="products" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="product_edit" data-url-search="{{ route('search_product', ['id' => $indexBar->id]) }}?q=%QUERY">
					        </div>
					      </div>
			      </div>
			    </div>

		      <div class="row">
		      	<div class="col-xs-12">
		      		<div class="table-responsive no-padding">
								<table class="table table-bordered">
									<tbody id="items">
										@foreach($order->items as $item)
											<tr class="item" id="tr-{{ $item->product->id }}">
					              <td class="name" width="55%">
					                {{ $item->product->name }}
					                <input name="product[]" type="hidden" id="product-input-{{ $item->product->id }}" value="{{ $item->product->id }}">
					              </td>
					              <td width="15%">
					                <div class="input-group">
					                  <span class="input-group-btn">
					                    <button class="btn btn-primary btn-flat btn-sm btn-minus" type="button" data-id="{{ $item->product->id }}"><i class="fa fa-minus"></i></button>
					                  </span>
					                  <small class="qtd" id="qtd-{{ $item->product->id }}">{{ $item->quantity }}</small>
					                  <input name="quantity[]" type="hidden" id="qtd-input-{{ $item->product->id }}" value="{{ $item->quantity }}">
					                  <span class="input-group-btn">
					                    <button class="btn btn-primary btn-flat btn-sm btn-plus" type="button" data-id="{{ $item->product->id }}"><i class="fa fa-plus"></i></button>
					                  </span> 
					                </div>
					              </td>
					              <td width="20%" class="name">
					                <span id="value-{{ $item->product->id }}">R$ {{ number_format($item->price * $item->quantity, 2, '.', ',') }}</span>
					                <input name="value[]" type="hidden" id="value-input-{{ $item->product->id }}" data-value="{{ $item->price }}" value="{{ $item->price }}">
					              </td>
					              <td width="10%">
					                <div class="btn-group" style="float:right;">
					                  <a href="#" class="btn btn-sm btn-danger remove-item" data-id="{{ $item->product->id }}"><i class="fa fa-trash-o"></i></a>
					                </div>
					              </td>
					            </tr>
										@endforeach
									</tbody>
								</table>
		      		</div>
		      	</div>
		      </div>

		      <hr />

		      @if( Auth::user()->isAdmin($indexBar) && $finished )
		      	<button type="submit" class="btn btn-primary btn-block">Finalizar</button>
		      @else
			      <button type="submit" class="btn btn-primary btn-block">Salvar</button>
		      @endif

		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop