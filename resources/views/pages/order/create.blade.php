@extends('layouts.normal.layout')

@section('title')
	Criar Pedido
@stop

@section('content-header-title')
	Criar Pedido
@stop

@section('script-script')
<script src="/js/app/order.js" type="text/javascript"></script>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
    <div class="row">
      <div class="col-lg-6 col-xs-12">
		  	<form method="post" action="{{ route('store_order') }}">

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
		        <label>Nome</label>
		        <input name="name" type="text" class="form-control" value="{{ old('name') }}" placeholder="{{ trans('messages.category.name') }}">
		        <span class="help-block text-error">{{ $errors->first('name') }}</span>
		      </div>

		  		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
		        <label>Mesa</label>
		        <select class="form-control" name="table">
                            <option value="0">Selecione...</option>
		        	@foreach($tables as $table)
              	<option value="{{ $table->id }}">{{ $table->name }}</option>
              @endforeach
            </select>
		        <span class="help-block text-error">{{ $errors->first('name') }}</span>
		      </div>

			    <div class="row">
			      <div class="col-lg-12 col-xs-12">
					  		<div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
					        <label>Produto</label>
					        <div class="input-group col-xs-12">
						        <input name="products" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="product_edit" data-url-search="{{ route('search_product', ['id' => $indexBar->id]) }}?q=%QUERY">
					        </div>
					      </div>
			      </div>
			    </div>

		      <div class="row">
		      	<div class="col-xs-12">
		      		<div class="table-responsive no-padding">
								<table class="table table-bordered">
									<tbody id="items">
									</tbody>
								</table>
		      		</div>
		      	</div>
		      </div>

		      <hr />

		      <button type="submit" class="btn btn-primary btn-block">{{ trans('messages.common.create_button') }}</button>
		      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	</form>
			</div>
		</div>
	</div>
</div><!-- /.row (main row) -->

@stop