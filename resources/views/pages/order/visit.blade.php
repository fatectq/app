@extends('layouts.auth.layout')

@section('title')
  Pedido #{{ $order->id }}
@stop

@section('header')
  Olá, {{ $order->client->name }}
@stop

@section('content')
<div class="body bg-gray">
  <div class="table-responsive no-padding">
    <table class="table">
      <tbody>
        @foreach($order->items as $item)
        <tr class="item">
          <td class="name">
            {{ $item->product->name }} <small class="badge pull-right bg-blue">{{ $item->quantity }}</small>
          </td>
          <td class="name">
            R$ {{ number_format($item->price * $item->quantity, 2, '.', ',') }}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  <hr />

  <div class="row">
    <div class="col-xs-12">
      <h4>
        @if( is_null($order->finished_at) )
          SUB-TOTAL: 
        @else
          TOTAL: 
        @endif
        R$ {{ number_format($order->total, 2, '.', ',') }}
      </h4>
    </div>
  </div>
  
  <hr />
  @if( !is_null($order->finished_at) )
    <a href="{{ route('enjoyment', [ 'token' => $order->token]) }}" class="btn btn-block btn-success">Avaliar o atendimento</a>
  @endif

  @if( empty($order->client->email) )
  <form id="form-add-email" method="post" action="" data-token="{{ $order->token }}">
    <div class="input-group input-group-sm">
      <input type="text" class="form-control" name="email" placeholder="E-mail">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-info btn-flat" id="email-go" type="button">Go!</button>
      </span>
    </div>
    <span class="help-block">Cadastre o seu e-mail para receber promoções</span>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>
  @endif

  <div class="alert alert-success alert-dismissable" id="msg-add-email" style="margin-left: 0 !important; display: none;">
    E-mail cadastrado com sucesso!
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  </div>

</div>

<script>
  window.ROUTES = {
    'add_email': '{{ route("add_email", ["id" => 0]) }}'
  };
</script>

@stop
     