@extends('layouts.normal.layout')

@section('title')
	Pedidos
@stop

@section('content-header-title')
	Pedidos
@stop

@section('script-script')
<script src="/js/app/common.js" type="text/javascript"></script>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">
		<!-- search form -->
    <div class="input-group col-xs-12">
      <input name="order" type="text" id="input-search" class="form-control typeahead" placeholder="Pesquisar" data-url="order_edit" data-url-search="{{ route('search_order', ['id' => $indexBar->id]) }}?q=%QUERY">
    </div>
	</div>
</div>

<hr />

<div class="row">
	<div class="col-lg-12 col-xs-12">
		<div class="table-responsive no-padding">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th>Nome</th>
						<th>Total</th>
						<th></th>
					</tr>
          @if(count($orders) != 0) 
						@foreach($orders as $order)
						<tr>
							<td class="name">{{ $order->client->name }}</td>
							<td class="name">R$ {{ number_format($order->total, 2, ',', '.') }}</td>
							<td>
								<div class="btn-group" style="float:right;">
									<a href="{{ route('edit_order', ['id' => $order->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
									@if( Auth::user()->isAdmin($indexBar) )
									<a href="{{ route('finish_order', ['id' => $order->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-adjust"></i></a>
									@endif									
								</div>
							</td>
						</tr>
						@endforeach
          @else
          <tr>
              <td colspan="4"><span style="color: red;">{{ trans('messages.search.no_result') }}</span></td>
          </tr>
          @endif
				</tbody>
			</table>
		</div>
		
	</div>
</div><!-- /.row (main row) -->

@stop