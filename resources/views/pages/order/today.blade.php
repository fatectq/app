@extends('layouts.normal.layout')

@section('title')
	Pedidos
@stop

@section('content-header-title')
	Pedidos
@stop

@section('content')

<div class="row">
	<div class="col-lg-12 col-xs-12">
		<div class="table-responsive no-padding">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th>Nome</th>
						<th>Total</th>
						<th></th>
					</tr>
          @if(count($orders) != 0) 
						@foreach($orders as $order)
						<tr>
							<td class="name">{{ $order->client->name }}</td>
							<td class="name">R$ {{ number_format($order->total, 2, ',', '.') }}</td>
							<td>
								<div class="btn-group" style="float:right;">
									<a href="{{ route('view_order', ['id' => $order->id]) }}" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
								</div>
							</td>
						</tr>
						@endforeach
          @else
          <tr>
              <td colspan="4"><span style="color: red;">{{ trans('messages.search.no_result') }}</span></td>
          </tr>
          @endif
				</tbody>
			</table>
		</div>
		
	</div>
</div><!-- /.row (main row) -->

@stop