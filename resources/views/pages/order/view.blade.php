@extends('layouts.normal.layout')

@section('title')
	Ver Pedido
@stop

@section('content-header-title')
	Ver Pedido
@stop

@section('content-header-extra')
<a href="{{ route('today_order') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</a>
@stop

@section('content')

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">

    <div class="table-responsive no-padding">
      <table class="table table-bordered table-hover">
        <tbody>
        	<tr>
            <th>Nome</th>
            <td>
              {{ $order->client->name }}
              @if( !empty($order->client->email) )
                ( <a href="mailto:{{ $order->client->email }}">{{ $order->client->email }}</a> )
              @endif
            </td>
          </tr>
          <tr>
            <th>Data do pedido</th>
            <td>
              {{ $order->created_at->format('d/m/Y') }}
            </td>
          </tr>
          <tr>
            <th>Total do pedido</th>
            <td>R$ {{ number_format($order->total, 2, ',', '.') }}</td>
          </tr>
          <tr>
            <th>Token</th>
            <td>
              {{ $order->token }} 
              <a href="{{ route('visit_order', ['token' => $order->token]) }}" class="btn btn-xs btn-success pull-right">Ver comanda <i class="fa fa-arrow-right"></i></a>
            </td>
          </tr>
        	<tr>
            <th>Avaliação</th>
            <td>
              @if( is_null($order->enjoyment) )
                Sem avaliação
              @else
                {{ $order->enjoyment->note }}
              @endif
            </td>
          </tr>
        </tbody>
      </table>
	  </div>
		
	</div>
</div><!-- /.row (main row) -->

<!-- Main row -->
<div class="row">
	<div class="col-lg-12 col-xs-12">

    <div class="table-responsive no-padding">
      <table class="table table-bordered table-hover">
        <thead>
        	<tr>
            <th>Produto</th>
            <th>Preço</th>
            <th>Qauntidade</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
        	@foreach($order->items as $item)
        	<tr>
            <td>{{ $item->product->name }}</td>
            <td>R$ {{ number_format($item->price, 2, ',', '.') }}</td>
            <td>{{ $item->quantity }}</td>
            <td>R$ {{ number_format($item->price * $item->quantity, 2, ',', '.') }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
	  </div>
		
	</div>
</div><!-- /.row (main row) -->

@stop