<h1>{{ trans('messages.auth.confirmation.subject') }}</h1>

<p>{{ trans('messages.auth.confirmation.greetings', array('name' => (isset($user['name'])) ? $user['name'] : $user['email'])) }},</p>

<p>{{ trans('messages.auth.confirmation.body') }}</p>
<a href='{{{ url('auth/confirm', ['code' => $user['confirmation_code']]) }}}'>
    {{ trans('messages.auth.confirmation.subject') }}
</a>

<p>{{ trans('messages.auth.confirmation.farewell') }}</p>