<a href="{{ route('dashboard') }}" class="logo">
  <!-- Add the class icon to your logo image or logo icon to add the margining -->
  <img src="/img/logo.png" />
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </a>
  <div class="navbar-right">
    <ul class="nav navbar-nav">

      <!-- Tasks: style can be found in dropdown.less -->
      @include('elements.layout.normal.header.bar')
      @include('elements.layout.normal.header.user')
    </ul>
  </div>
</nav>