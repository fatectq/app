
<script src="/js/plugins/jquery/jquery.min.js"></script>
<script src="/js/plugins/jquery/jquery.maskMoney.js" type="text/javascript"></script>
<script src="/js/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/plugins/jqueryui/jquery-ui.min.js" type="text/javascript"></script>
@yield('script-plugin')
<!-- Sparkline -->
<!-- <script src="/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>-->
<!-- jvectormap -->
<!-- <script src="/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>-->
<!-- <script src="/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>-->
<!-- jQuery Knob Chart -->
<!-- <script src="/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>-->
<!-- datepicker -->
<script src="/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/js/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<!-- input-mask -->
<script src="/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
<script src="/js/plugins/typeahead/typeahead.js" type="text/javascript"></script>

<script src="/js/app/config.js" type="text/javascript"></script>
<script src="/js/app/movement.js" type="text/javascript"></script>
<script src="/js/app/global.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="/js/AdminLTE/demo.js" type="text/javascript"></script>

@yield('script-script')

<!-- app -->