@include('layouts.normal.sidebar')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row header">
      <div class="col-lg-10 col-xs-6 text-left">
        <h1>
          @yield('content-header-title')
        </h1>
      </div>
      <div class="col-lg-2 col-xs-6 text-right">
          @yield('content-header-extra')
      </div>
    </div><!-- /.row (main row) -->

    <hr />

    @yield('content')
  </section><!-- /.content -->
</aside><!-- /.right-side -->