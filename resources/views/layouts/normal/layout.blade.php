<!DOCTYPE html>
<html>
  <head>
    @include('layouts.normal.head')
    @include('elements.layout.normal.data_js')
  </head>
  <body class="skin-blue fixed">
    <!-- header logo: style can be found in header.less -->
    <header class="header">
        @include('layouts.normal.header')
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        @include('layouts.normal.wrapper')
    </div><!-- ./wrapper -->

    <!-- add new calendar event modal -->

    @include('layouts.normal.script')

  </body>
</html>