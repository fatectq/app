<!DOCTYPE html>
<html>
  <head>
    @include('layouts.clean.head')
  </head>
  <body class="skin-blue fixed">
    <!-- header logo: style can be found in header.less -->
    <header class="header">
        @include('layouts.clean.header')
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        @include('layouts.clean.wrapper')
    </div><!-- ./wrapper -->

    @include('layouts.clean.script')

  </body>
</html>