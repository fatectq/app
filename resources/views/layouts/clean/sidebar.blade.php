<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas inactivate">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="">
        <a href="{{ URL::route('dashboard') }}">
          <i class="fa fa-home"></i> <span>Visão geral</span>
        </a>
      </li>

      <li class="">
        <a href="{{ route('create_order') }}">
          <i class="fa fa-plus"></i> <span>Novo Pedido</span>
        </a>
      </li>

      <li class="">
        <a href="{{ route('create_order') }}">
          <i class="fa fa-search"></i> <span>Buscar Pedido</span>
        </a>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-barcode"></i>
          <span>Produtos</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>

        <ul class="treeview-menu">
          <li><a href="{{ route('products') }}">
                <i class="fa fa-angle-double-right"></i> <span>Produtos</span>
              </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-map-marker"></i>
          <span>Mesas</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>

        <ul class="treeview-menu">
          <li><a href="{{ route('tables') }}">
              <i class="fa fa-angle-double-right"></i> <span>Mesas</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-list"></i>
          <span>Categorias</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        
        <ul class="treeview-menu">
          <li><a href="{{ route('categories') }}">
              <i class="fa fa-angle-double-right"></i> <span>Categorias</span>
            </a>
            </li>
        </ul>
      </li>
      <li class="treeview">
          <a href="#">
              <i class="fa fa-ticket"></i>
              <span>Promoções</span>
              <i class="fa fa-angle-left pull-right"></i>
          </a>

          <ul class="treeview-menu">
              <li><a href="{{ route('promotions') }}">
                      <i class="fa fa-angle-double-right"></i> <span>Promoções</span>
                  </a>
              </li>
          </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-bar-chart-o"></i>
          <span>Relatórios</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/morris.html"><i class="fa fa-angle-double-right"></i> Pedidos</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-angle-double-right"></i> Produtos</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-angle-double-right"></i> Promoções</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-building"></i>
          <span>Estabelecimentos</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('bars') }}"><i class="fa fa-angle-double-right"></i> Estabelecimentos</a></li>
        </ul>
      </li>
      
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>