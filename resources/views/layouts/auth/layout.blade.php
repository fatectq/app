<!DOCTYPE html>
<html class="bg-black">
  <head>
    <meta charset="UTF-8">
    <title>NightCheers | @yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/fontawesome/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- icheck -->
    <link href="/css/iCheck/all.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/css/AdminLTE.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="bg-black">

    <div class="form-box" id="login-box">
      <div class="header">@yield('header')</div>
      @yield('content')

      <!--<div class="margin text-center">
        <span>Sign in using social networks</span>
        <br/>
        <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
        <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
        <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

      </div>-->
    </div>

    <script src="/js/plugins/jquery/jquery.min.js"></script>
    <script src="/js/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script src="/js/app/config.min.js" type="text/javascript"></script>

  </body>
</html>
