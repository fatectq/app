<div class="row">
  <div class="col-lg-12 col-xs-12">

    <div class="table-responsive no-padding">
      <table class="table">
        <thead>
          <tr>
            <th>Total</th>
            <th>Data do pedido</th>
            <th>Qtd. de itens</th>
          </tr>
        </thead>
        <tbody>
          @foreach($orders as $order)
          <tr>
            <td>{{ $order->total }}</td>
            <td>{{ $order->created_at }}</td>
            <td>{{ $order->items->count() }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>