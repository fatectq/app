<div class="row">
	<div class="col-lg-12 col-xs-12">

  	<div class="table-responsive no-padding">
      <table class="table table-bordered">
        <thead>
        	<tr>
            <th>Produto</th>
            <th>Preço de compra</th>
            <th>Preço de venda</th>
            <th>Qtd. vendida</th>
          </tr>
        </thead>
        <tbody>
        	@foreach($products as $product)
        	<tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->purchase_value }}</td>
            <td>{{ $product->sale_value }}</td>
            <td>{{ $product->items->count() }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
	  </div>

	</div>
</div>