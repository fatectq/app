<div class="row">
  <div class="col-lg-12 col-xs-12">

    <div class="table-responsive no-padding">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Data de Inicio</th>
            <th>Data de Término</th>
            <th>Cliente</th>
            <th>Desconto</th>
          </tr>
        </thead>
        <tbody>
          @foreach($promotions as $promotion)
          <tr>
            <td>{{ $promotion->start_date }}</td>
            <td>{{ $promotion->end_date }}</td>
            <td>{{ $promotion->client->name }}</td>
            <td>{{ $promotion->discount }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>