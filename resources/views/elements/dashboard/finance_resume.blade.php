<!-- Custom tabs (Charts with tabs)-->
<div class="box">
  <!-- Tabs within a box -->
  <div class="box-header">
    <i class="fa fa-bar-chart-o"></i> 
    <h3 class="box-title">Resumo anual</h3>
  </div>
  <div class="box-body">
  	<!-- <div style="background: url('/img/graph-home.jpg') no-repeat; height: 240px; width: 100%;" >
  		<p style="padding-top: 90px; text-align: center; font-weight: bold; font-size: 12pt;">
  			<a href="#">Comece agora!</a> Adicione suas movimentações e veja seu desempenho.
  		</p>
  	</div> -->
    <!-- Morris chart - Sales -->
    <canvas id="chart" style="width:100%; height:250px;"></canvas>
  </div>
</div><!-- /.nav-tabs-custom -->