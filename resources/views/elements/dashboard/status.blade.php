<div class="col-lg-6 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-light-blue">
    <div class="inner">
      <h3>
        R$ {{ number_format($total, 2, ',', '.') }}
      </h3>
      <p>
        Total do dia
      </p>
    </div>
    <a href="{{ route('today_order') }}" class="small-box-footer">
      {{ trans('messages.dashboard.details') }} <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->
<div class="col-lg-6 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-green">
    <div class="inner">
      <h3>
        {{ $numberOrders }}
      </h3>
      <p>
        Total de pedidos do dia
      </p>
    </div>
    <a href="{{ route('today_order') }}" class="small-box-footer">
      {{ trans('messages.dashboard.details') }} <i class="fa fa-arrow-circle-right"></i>
    </a>
  </div>
</div><!-- ./col -->