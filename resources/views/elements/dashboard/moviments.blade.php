<div class="nav-tabs-custom">
  <!-- Tabs within a box -->
  <ul class="nav nav-tabs pull-right">
    <li><a href="#expense" data-toggle="tab">Despesa</a></li>
    <li class="active"><a href="#revenue" data-toggle="tab">Receita</a></li>
    <li class="pull-left header"><i class="fa fa-list"></i> Movimentações</li>
  </ul>
  <div class="tab-content no-padding">
    <!-- Morris chart - Sales -->
    <div class="tab-pane active" id="revenue">
      @include('elements.dashboard.elements.revenue')
    </div>
    <div class="tab-pane" id="expense">
      @include('elements.dashboard.elements.expensive')
    </div>
  </div>
</div>