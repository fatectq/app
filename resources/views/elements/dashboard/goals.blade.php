<!-- Calendar -->
<div class="box">
  <div class="box-header">
    <i class="fa fa-check"></i>
    <h3 class="box-title">Metas</h3>
  </div><!-- /.box-header -->
  <div class="box-body text-black">
    <div class="row">
      @if ( count($goals) == 0 )
        <div class="col-sm-12">
          <div class="callout callout-warning">
            <p>
              {{ trans('messages.dashboard.without_goals') }}
              <a href="{{ route('create_goal') }}">{{ trans('messages.dashboard.without_goals_link') }}</a>
            </p>
          </div>
        </div>
      @else
        <div class="col-sm-12">
          <!-- Progress bars -->
          <div class="clearfix">
            <span class="pull-left">Outras coisas</span>
            <small class="pull-right">90%</small>
          </div>
          <div class="progress xs">
            <div class="progress-bar progress-bar-warning" style="width: 90%;"></div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="clearfix">
            <span class="pull-left">Educação</span>
            <small class="pull-right">70%</small>
          </div>
          <div class="progress xs">
            <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
          </div>
        </div><!-- /.col -->
        <div class="col-sm-12">
          <div class="clearfix">
            <span class="pull-left">Transporte</span>
            <small class="pull-right">60%</small>
          </div>
          <div class="progress xs">
            <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="clearfix">
            <span class="pull-left">Saúde</span>
            <small class="pull-right">15%</small>
          </div>
          <div class="progress xs">
            <div class="progress-bar progress-bar-primary" style="width: 15%;"></div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="clearfix">
            <span class="pull-left">Alimentação</span>
            <small class="pull-right">101%</small>
          </div>
          <div class="progress xs">
            <div class="progress-bar progress-bar-danger" style="width: 101%;"></div>
          </div>
        </div><!-- /.col -->
      @endif
    </div><!-- /.row -->                                                                        
  </div>
</div><!-- /.box -->