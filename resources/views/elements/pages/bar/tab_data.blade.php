<form method="post" action="{{ route('update_bar', ['slug' => $editBar->id]) }}">

	<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label>Nome</label>
    <input name="name" type="text" class="form-control" value="{{ $editBar->name }}">
    <span class="help-block text-error">{{{ $errors->first('name') }}}</span>
  </div>

  <button type="submit" class="btn btn-primary">{{ trans('messages.common.save_button') }}</button>
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>