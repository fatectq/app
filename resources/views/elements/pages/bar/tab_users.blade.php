<div class="col-xs-12">
	<label>Usuários vinculados ao estabelecimento</label>
	<div class="table-responsive no-padding bg-white">
		<table class="table">
			<tbody id="users-bar">
	  		@foreach($usersBar as $userBar)
					<tr class="bar-user" id="{{ $userBar->id }}">
						<td class="name">{{ $userBar->name }}</td>
						<td>
							<div class="btn-group" style="float:right;">
								@if($editBar->owner_id != $userBar->id)
								<a href="#" class="btn btn-sm 
									@if($userBar->isAdmin($editBar)) btn-success @else btn-default @endif admin-user" data-email="{{ $userBar->email }}" data-user="{{ $userBar->id }}"><i class="fa fa-plus"></i></a>
								@endif
								@if(Auth::user()->id != $userBar->id)
								<a href="#" class="btn btn-sm btn-default remove-user" data-email="{{ $userBar->email }}" data-user="{{ $userBar->id }}"><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
	  		@endforeach
			</tbody>
		</table>
	</div>

	<div class="alert alert-danger alert-dismissable" id="error-remove-user" style="margin-left: 0 !important; display: none;">
    Não foi possível remover o usuário
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  </div>

	<div class="alert alert-danger alert-dismissable" id="error-admin-user" style="margin-left: 0 !important; display: none;">
    Não foi possível dar previlégios de admin para o usuário
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  </div>

	<hr />

	<form method="post" id="form-add-user-bar">

		<div class="form-group {{ $errors->has('user') ? 'has-error' : '' }}}">
	    <label>Adicionar usuário</label>
			<div class="input-group">
	      <input name="user" type="text" id="input-user" class="form-control typeahead" placeholder="Email do usuário" data-url-search="{{ route('search_user', ['id' => $editBar->id]) }}?q=%QUERY">
	      <span class="input-group-btn">
	    		<button type="submit" class="btn btn-primary" id="add-user">Adicionar</button>
	      </span>
	    </div>
	    <span class="help-block text-error">{{{ $errors->first('user') }}}</span>
	  </div>

	  <input type="hidden" name="bar" id="bar-id" value="{{ $editBar->id }}">
	  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	</form>

	<div class="alert alert-danger alert-dismissable" id="error-add-user" style="margin-left: 0 !important; display: none;">
	  Não foi possível adicionar o usuário
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	</div>

</div>