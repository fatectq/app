<script>
  window.TOKEN = '{{ csrf_token() }}';

  window.IS_DASHBOARD = {{ Request::is( str_replace("/","",route("dashboard", [], false)) ) ? 'true' : 'false' }};

  window.ROUTES = {
    'add_user_bar': '{{ route("add_user_bar") }}',
    'remove_user_bar': '{{ route("destroy_user_bar") }}',
    'admin_user_bar': '{{ route("admin_user_bar") }}',
    'product_edit': '{{ route("edit_product", ["id" => 0]) }}',
    'category_edit': '{{ route("edit_category", ["id" => 0]) }}',
    'promotion_edit': '{{ route("edit_promotion", ["id" => 0]) }}',
    'table_edit': '{{ route("edit_table", ["id" => 0]) }}',
    'order_edit': '{{ route("edit_order", ["id" => 0]) }}',
    'add_email': '{{ route("add_email", ["id" => 0]) }}'
  };
</script>