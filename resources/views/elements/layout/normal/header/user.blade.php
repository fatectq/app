<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <span class="glyphicon glyphicon-user"><i class="caret"></i></span>
    <!--<i class="glyphicon glyphicon-user"></i>
    <span>Jane Doe <i class="caret"></i></span>-->
  </a>
  <ul class="dropdown-menu">
    <!-- Menu Footer-->
    <li class="user-footer">
      <div class="pull-right">
        <a href="{{ route('logout') }}" class="btn btn-default btn-flat">{{ trans('messages.dashboard.logout') }}</a>
      </div>
    </li>
  </ul>
</li>