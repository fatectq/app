<li class="dropdown accounts-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-random"></i>
  </a>
  <ul class="dropdown-menu">
    <li class="header">{{ trans('messages.header.accounts.total', ['total' => $total_account]) }}</li>
    <li>
      <ul class="menu">
      @foreach( $accounts as $acc )
        <li class="row"><!-- Task item -->
          <a href="{{ route('change_account', ['slug' => $acc->slug ]) }}" class="account-name col-xs-10">
            <h3 class="{{ ( $account->id === $acc->id ) ? 'text-bold' : '' }}">
              {{ $acc->name }}
            </h3>
          </a>
          <a href="{{ route('edit_account', ['slug' => $acc->slug ]) }}" class="account-edit col-xs-2"><i class="fa fa-edit"></i></a>
        </li><!-- end account item -->
      @endforeach
      </ul>
    </li>
    <li class="footer">
      <a href="{{ route('accounts') }}">{{ trans('messages.header.accounts.all') }}</a>
    </li>
  </ul>
</li>