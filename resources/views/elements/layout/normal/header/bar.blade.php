<li class="dropdown accounts-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-random"></i>
  </a>
  <ul class="dropdown-menu">
    <li class="header">
      <b>Total: {{ 2 }}</b>
      <a href="{{ route('create_bar') }}" class="pull-right btn btn-primary btn-xs" style="color: #fff;">Novo</a>
    </li>
    <li>
      <ul class="menu">
      @foreach( $bars as $acc )
        <li class="row"><!-- Task item -->
          <a href="{{ route('change_bar', ['id' => $acc->id ]) }}" class="account-name col-xs-10">
            <h3 class="{{ ( $bar->id === $acc->id ) ? 'text-bold' : '' }}">
              {{ $acc->name }}
            </h3>
          </a>
          @if( $acc->isAdmin() )
          <a href="{{ route('edit_bar', ['id' => $acc->id ]) }}" class="account-edit col-xs-2"><i class="fa fa-edit"></i></a>
          @endif
        </li><!-- end account item -->
      @endforeach
      </ul>
    </li>
    <li class="footer">
      <a href="{{ route('bars') }}">Ver todos os estabelecimentos</a>
    </li>
  </ul>
</li>