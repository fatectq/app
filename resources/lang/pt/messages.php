﻿<?php
return [

    'common' => [
        'create_button' => 'Criar',
        'save_button' => 'Salvar',
        'cancel_button' => 'Cancelar',
        'add_button' => 'Adicionar'
    ],
    'sidebar' => [
        'dashboard' => 'Visão geral',
        'message' => 'Mensagens',
        'calendar' => 'Calendário',
        'setting' => 'Configurações',
        'report' => 'Relatórios',
        'category' => 'Categorias',
        'promotions' => 'Promoções'
    ],
    'header' => [
        'accounts' => [
            'all' => 'Ver todas as contas',
            'total' => 'Você tem :total conta(s)'
        ]
    ],
    'auth' => [
        'success_singup' => 'Cadastro realizado com sucesso!',
        'success_confirm' => 'Usuário confirmado com sucesso!',
        'error_confirm' => 'Usuário já confirmado ou código de confirmação inválido!',
        'error_singin' => 'Email e/ou senha não conferem!',
        'login' => 'Entrar',
        'register' => 'Registrar',
        'forgotten_password' => 'Esqueceu a senha ?',
        'keep_me_conected' => 'Mantenha-me conectado',
        'email_placeholder' => 'E-mail',
        'password_placeholder' => 'Senha',
        'name_placeholder' => 'Nome',
        'password_again_placeholder' => 'Insira novamente a senha',
        'new_user' => 'Novo Usuário',
        'create' => 'Criar',
        'confirmation' => [
            'subject' => 'Confirmação de conta',
            'greetings' => 'Olá :name',
            'body' => 'Por favor, acesse o link abaixo para confirmar a sua conta',
            'farewell' => 'Att',
        ]
    ],
    'account' => [
        'edit_header' => 'Editar conta: :name',
        'name' => 'Nome',
        'value' => 'Valor',
        'value_example' => '00,00',
        'users' => 'Usuários vinculados a conta',
        'add_user' => 'Adicionar usuário',
        'add_user_input' => 'Email do usuário',
        'name_help' => 'Por exemplo: Casa, Pessoal, Familia e etc.',
        'value_help' => 'Caso a conta ja tenha alguma receita.',
        'header_accounts' => 'Contas',
        'all_tab' => 'Todas contas',
        'your_tab' => 'Suas contas',
        'error_add_user' => 'Não foi possível adicionar o usuário',
        'error_remove_user' => 'Não foi possível remover o usuário',
    ],
    'category' => [
        'edit_header' => 'Editar conta: :name',
        'name' => 'Nome',
        'group' => 'Grupo',
        'header_accounts' => 'Categorias',
        'group_help' => 'Escolha um grupo ao qual sua categoria pertença.',
        'groups' => [
            'health' => 'Saúde',
            'education' => 'Educação',
            'habitation' => 'Habitação',
            'transport' => 'Transporte',
            'others' => 'Outros'
        ]
    ],
    'dashboard' => [
        'title' => 'Visão geral',
        'logout' => 'Sair',
        'without_goals' => 'Não há nenhuma meta cadastrada.',
        'without_goals_link' => 'Cadastre agora!',
        'value_format' => 'R$ :value',
        'revenue' => 'Receita',
        'expensive' => 'Despesa',
        'balance' => 'Saldo',
        'details' => 'Detalhes',
    ],
    'movement' => [
        'title' => 'Adicionar movimentação',
        'name' => 'Descrição',
        'category' => 'Categoria',
        'type' => 'Tipo',
        'date' => 'Data',
        'value' => 'Valor',
        'attachment' => 'Anexo',
        'attachment_help' => 'Max. 2MB / .jpg e .png',
        'repeat' => 'Repetir',
        'fixed_repeat' => 'É uma movimentação fixa',
        'continue_repeat' => 'É uma movimentação parcelada',
        'continue_repeat_help' => 'Número de vezes que o valor é repetido',
        'expensive' => 'Despesa',
        'revenue' => 'Receita',
        'add' => 'Adicionar',
        'cancel' => 'Cancelar',
    ],
    'promotion' => [
        'edit_header' => 'Editar conta: :name',
        'name' => 'Bar',
        'client' => 'Cliente',
        'star_date' => 'Data Inicial',
        'end_date' => 'Data Final',
        'header_accounts' => 'Promoções',
    ],
    
    'search' => [
        'no_result' => 'Resultado não encontrado!'
    ]
];